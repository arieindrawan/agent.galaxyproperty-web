<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\GstController;
use App\Http\Controllers\VidioController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\OpenHouseController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home.home');
});
Route::get('/', [HomeController::class, 'index'])->name('home');
//Route::get('/home', [HomeController::class, 'index'])->name('home');

//GST
Route::middleware(['auth:sanctum', 'verified'])->get('/gst', function () {
    return view('pages/gst/index');
})->name('gst');
Route::middleware(['auth:sanctum', 'verified'])->get('/gst/report', function () {
    return view('pages/gst/report');
})->name('gst/report');
Route::get('/gst', [GstController::class, 'index'])->name('gst');
Route::get('/gst/single_vidio/{id}', [GstController::class, 'singleVidio'])->name('gst/single_vidio');
Route::post('/gst/updateReport', [GstController::class, 'updateReport'])->name('update_report_gst');
Route::get('/gst/getAgentReport', [GstController::class, 'getAgentReport'])->name('gst/getAgentReport');
Route::get('/gst/getVidioReport', [GstController::class, 'getVidioReport'])->name('gst/getVidioReport');
Route::get('/gst/getStatistic', [GstController::class, 'getStatistic'])->name('gst/getStatistic');
Route::get('/gst/detail_agent/{id}', [GstController::class, 'detailAgent'])->name('gst/detail_agent');
Route::get('/gst/detail_vidio/{id}', [GstController::class, 'detailVidio'])->name('gst/detail_vidio');


//PROPERTY
Route::get('/property', [PropertyController::class, 'index'])->name('property');
Route::get('/property/getCity', [PropertyController::class, 'getCity'])->name('property/getCity');
Route::get('/property/getProvince', [PropertyController::class, 'getProvince'])->name('property/getProvince');
Route::get('/property/getPropertyPDF', [PropertyController::class, 'createPDF'])->name('property/getPropertyPDF');
Route::get('/property/getPropertyAPI', [PropertyController::class, 'getPropertyAPI'])->name('property/getPropertyAPI');



//VIDIO
Route::resource('vidios', VidioController::class);

//OPEN HOUSE
Route::get('/open-house', [OpenHouseController::class, 'index'])->name('open-house');
Route::get('/open-house-galaxy/{id}', [OpenHouseController::class, 'getByRegion'])->name('open-house-galaxy');
Route::get('/open-house/city/{id}', [OpenHouseController::class, 'getByProvince'])->name('open-house/city');
Route::get('/open-house/create', [OpenHouseController::class, 'create'])->name('open-house/create');
Route::get('/open-house/list-property', [OpenHouseController::class, 'listProperty'])->name('open-house/list-property');
Route::post('/open-house/destroy/{id}', [OpenHouseController::class, 'destroy'])->name('open-house/destroy');
Route::get('/open-house/store/{id}', [OpenHouseController::class, 'store'])->name('open-house/store');
Route::get('/open-house/cetak-listing', [OpenHouseController::class, 'cetakListing'])->name('open-house/cetak-listing');
Route::get('/open-house/export/{id}', [OpenHouseController::class, 'export'])->name('open-house/export');

//EMAIL
Route::get('/home/sendEmail', [HomeController::class, 'sendEmail'])->name('home/sendEmail');

//WISHLIST
//Route::resource('wishlist', WishlistController::class);
Route::get('/wishlist', [WishlistController::class, 'index'])->name('wishlist');
Route::get('/wishlist/create', [WishlistController::class, 'create'])->name('wishlist/create');
Route::post('/wishlist/destroy/{id}', [WishlistController::class, 'destroy'])->name('wishlist/destroy');
Route::post('/wishlist/store', [WishlistController::class, 'store'])->name('wishlist/store');
Route::post('/wishlist/update', [WishlistController::class, 'update'])->name('wishlist/update');
Route::get('/wishlist/edit/{id}', [WishlistController::class, 'edit'])->name('wishlist/edit');
Route::get('/wishlist/addToWishlist', [WishlistController::class, 'addToWishlist'])->name('wishlist/addToWishlist');
Route::get('/wishlist/addToWishlistModal', [WishlistController::class, 'addToWishlistModal'])->name('wishlist/addToWishlistModal');