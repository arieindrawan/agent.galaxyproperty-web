<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyOpenhousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_openhouses', function (Blueprint $table){
            $table->integer('id')->nullable();
            $table->string('number',20)->nullable();
            $table->string('title',255)->nullable();
            $table->text('description')->nullable();
            $table->decimal('price',30,2)->nullable();
            $table->float('land_area')->nullable();
            $table->float('building_area')->nullable();
            $table->string('listing_type',20)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('transaction_type',20)->nullable();
            $table->integer('propertycategory_id')->nullable();
            $table->integer('certificate_id')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->string('region',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('toward',20)->nullable();
            $table->integer('bedroom')->nullable();
            $table->integer('bathroom')->nullable();
            $table->boolean('has_additional_bedroom')->nullable();
            $table->boolean('has_additional_bathroom')->nullable();
            $table->integer('floor')->nullable();
            $table->string('status',20)->nullable();
            $table->text('images')->nullable();
            $table->text('gallery')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->boolean('is_deleted')->nullable();
            $table->boolean('is_hot')->nullable();
            $table->boolean('is_hot_active')->nullable();
            $table->integer('hot_ordering')->nullable();
            $table->datetime('approve_hot_at')->nullable();
            $table->string('type',20)->nullable();
            $table->text('knowledge')->nullable();
            $table->string('block',255)->nullable();
            $table->string('alley',255)->nullable();
            $table->string('house_number',255)->nullable();
            $table->text('additional_info')->nullable();
            $table->timestamps();
            $table->integer('nonactivereason_id')->nullable();
            $table->text('nonactive_note')->nullable();
            $table->string('trx_id',255)->nullable();
            $table->longtext('brochure')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_openhouses');
    }
}
