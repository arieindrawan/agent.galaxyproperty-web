@extends('layouts.app')
@section('content')
    <div class="modal fade" id="modalDetailAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content" >
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Agent</h5>
            </div>
            <div class="modal-body">
                  <table class="table table-striped" style="width:100%" id="table-detail-agent">
                    <thead>
                      <th>Judul Vidio</th>
                      <th>Ditonton</th></thead>
                    <tbody>
                        
                    </tbody>
                  </table> 
            </div>
            <div class="modal-footer">
              <div class="col-md-12">
                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="modal fade" id="modalDetailVidio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content" >
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Vidio</h5>
            </div>
            <div class="modal-body">
                  <table class="table table-striped" style="width:100%" id="table-detail-vidio">
                    <thead>
                      <th>Nama Agent</th>
                      <th>Menonton</th>
                    </thead>
                  <tbody></tbody>
                  </table> 
            </div>
            <div class="modal-footer">
              <div class="col-md-12">
                <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal">Tutup</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="ftco-blocks-cover-1">
      <div class="site-section overlay site-section-sm" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
          </div>
        </div>
      </div>
    </div>
      <div class="site-section site-section-sm" style="background-color: transparent;">
        <div class="container">
          <div class="realestate-filter nav " style="background-color: transparent;">
            <a href="#" class="active" data-toggle="tab" id="agent-report" aria-controls="rent" aria-selected="true">Agent Report</a>
            <a href="#" class="" data-toggle="tab" id="vidio-report" aria-controls="sale" aria-selected="false">Vidio Report</a>
            <a href="#" class="" data-toggle="tab" id="statistic" aria-controls="sale" aria-selected="false">Statistic</a>
          </div>
          <div class="row" style="margin-top: 1em;">
            <div class="container">
                <div class="container  result" style=" background-color:  #F8F8FF;">
                </div>
              </div>
          </div>
        </div>
      </div>
  
@endsection
@section('js_extras')
<script type="text/javascript">
  $(document).ready(function() {
    var dataPoints = [];
    $.ajax({
          url: "{{route('gst/getAgentReport')}}",
          success: function(data) {
            $(".result").append(data);
            $("#mytable").DataTable();
          }
        });
    $("#agent-report").click(function() {
        $.ajax({
          url: "{{route('gst/getAgentReport')}}",
          success: function(data) {
            $(".result").html(data);
            $("#mytable").DataTable();
          }
        });
    });
    $("#vidio-report").click(function() {
         $.ajax({
          url: "{{route('gst/getVidioReport')}}",
          success: function(data) {
            $(".result").html(data);
            $("#mytable").DataTable();
          }
        });
    });
    $("#statistic").click(function() {
      $.ajax({
          url: "{{route('gst/getStatistic')}}",
          success: function(data) {
            console.log(data);
            viewChart(data);
          }
        });
     
    });
});

    function detailAgent(link){
      $("#modalDetailAgent").modal("show");
      $.ajax({
          url: link,
          success: function(data) {
            var tableBody = $("#table-detail-agent tbody");
            tableBody.empty();
            $.each(data, function(idx, elem){
                tableBody.append("<tr><td>"+elem.vidio_name+"</td><td>"+elem.user_menonton+" kali</td></tr>");
            });
          }
        });
    }
    function detailVidio(link){
      $("#modalDetailVidio").modal("show");
      $.ajax({
          url: link,
          success: function(data) {

            var table = $("#table-detail-vidio tbody");
            table.empty();
            $.each(data, function(idx, elem){
                table.append("<tr><td>"+elem.user_name+"</td><td>"+elem.user_menonton+" kali</td></tr>");
            });
          }
        });
    }
    function getStatisticReport(){
      $.ajax({
          type: "get",
          url: "{{route('gst/getStatistic')}}",
          data: {
            'start' : $("#start_date").val(),
            'end' : $("#end_date").val(),
            "_token": "{{ csrf_token() }}"
          },
          success: function(data) {
            console.log(data);
            viewChart(data);
          }
        });
    }
    function getAgentReport(){
      //alert("hallo");
      $.ajax({
          type: "get",
          url: "{{route('gst/getAgentReport')}}",
          data: {
            'start' : $("#start_date").val(),
            'end' : $("#end_date").val(),
            "_token": "{{ csrf_token() }}"
          },
          success: function(data) {
            $(".result").html(data);
          }
        });
    }
    function viewChart(data){
       var str ="<h3 class='row align-items-center justify-content-center'>GST Activity Report</h3>"+  
             "<div class='row'>"+
                      "<div class='col-md-3 form-group'>"+
                      "<label class='control-label' for='start_date'>Start"+
                                  "</label>"+
                       "<input id='start_date' class='form-control' name='start_date' placeholder='e.g ' type='date' value=''>"+
                     "</div>"+
                     "<div class='col-md-3 form-group'>"+
                     "<label class='control-label' for='end_date'>End"+
                                  "</label>"+
                       "<input id='end_date' class='form-control' name='end_date' placeholder='e.g ' type='date' value=''>"+
                     "</div>"+
                     "<div class='col-md-3 form-group'>"+
                        "<button style='margin-top:2.5em;' onclick=getStatisticReport() type='button' class='btn btn-primary text-white px-2 py-2'>Filter</button>"+
                     "</div>"+
                   "</div>"+
                   "<canvas id='myChart'></canvas>";
                   $(".result").html(str);
                                     
                     var ctx = document.getElementById('myChart').getContext('2d');
                     var chart = new Chart(ctx, {
                         // The type of chart we want to create
                         type: 'bar',
             
                         // The data for our dataset
                         data: {

                             labels: ['00:00-03:00', '03:00-06:00', '06:00-09:00', '09:00-12:00', '12:00-15:00', '15:00-18:00', '18:00-21:00', '21:00-24:00'],
                             datasets: [{
                                 label: 'Jumlah',
                                 backgroundColor: 'rgb(193, 0, 22)',
                                 borderColor: 'rgb(193, 0, 22)',
                                 data: data
                             }]
                         },
             
                         // Configuration options go here
                         options: {}
                     }); 
    }
</script>
@endsection
    
