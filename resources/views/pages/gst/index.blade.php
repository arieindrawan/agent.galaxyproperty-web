@extends('layouts.app')
@section('style')
<style>

        @media screen and (min-width: 601px) {

        }

        @media screen and (max-width: 600px) {

          #vidio {
                display:none;
            }
        }
  </style>
@endsection
@section('content')
<div class="ftco-blocks-cover-1">
      <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-8">
              <h1 class="mb-2">SELAMAT DATANG PESERTA GST</h1>
              <p class="text-left mb-5">
                
                      <span>
                          <ul style="color: white; text-align: left;">
                            <li>Property Consultant yang baru bergabung silahkan langsung belajar melalui vidio berikut</li>
                            <li>WAJIB mengikuti sesi Tanya Jawab(Q&A) by ZOOM</li>
                            <li>Sesi Q&A diadakan 1x tiap bulan di minggu ke 3</li>
                            <li>Penjelasan sistem BOS GALAXY akan dijelaskan di sesi ini</li>
                            <li>Jadwal akan diumumkan di grup Telegram info Sharing</li>
                          </ul>  
                      </span>
              </p>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
          <div class="row" style="margin: 4em;">
              <h2>Vidio GST</h2>
          </div>                 
              <div class="wrapper-custom" style="margin-left: 4em; margin-right: 4em; width: 100%;">
                      <table id="mytable" class="table table-striped table-hover" style="cursor: pointer;">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Duration</th>
                            <th id="vidio"></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($vidios as $vidio)
                          <tr class='clickable-row ' data-href="{{route('gst/single_vidio', ['id'=>$vidio->id])}}">
                              <td width="10%" class="align-middle">
                                  {{ $vidio->order }}
                              </td>
                              <td width="50%" class="align-middle">
                                  {{ $vidio->name }}
                              </td>
                              <td width="20%" class="align-middle">
                                 {{ $vidio->duration }}
                              </td>
                              <td id="vidio" width="20%" height="130px" class="align-middle">
                                <img width="100%" height="100%" src="http://img.youtube.com/vi/{{$vidio->code}}/hqdefault.jpg">
                                  
                              </td>
                          </tr>
                        @endforeach
                        </tbody>
                      </table>
                      
                      <div class="clear"></div>
                  <div class="clear"></div>
              <div class="clear"></div>
            </div>
    </div>
    @endsection
    @section('js_extras')
    <script type="text/javascript">
      $(document).ready(function() {

        $("#mytable").DataTable();
       jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.open($(this).data("href"));
            });
        });
     });
    </script>
    @endsection
    
