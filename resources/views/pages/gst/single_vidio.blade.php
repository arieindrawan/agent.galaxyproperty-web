@extends('layouts.app')
@section('content')
<div class="ftco-blocks-cover-1">
      <div class="site-section overlay site-section-sm" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
          </div>
        </div>
      </div>
    </div>
<div class="ftco-blocks-cover-1">
    <div class="site-section-cover">
          <input id="duration" class="form-control col-md-12 col-xs-12" name="duration" type="hidden" value="{{@$vidios->duration}}">
          <input id="id_report" class="form-control col-md-12 col-xs-12" name="id_report" type="hidden" value="{{@$report->id}}">
          <iframe width="100%" height="100%"
            src="https://www.youtube.com/embed/{{ $vidios->code }}" allowfullscreen="allowfullscreen" >
          </iframe>
    </div>
</div>
    
@endsection
@section('js_extras')
    <script type="text/javascript">
      $(document).ready(function() {

      var duration = strToSecond($('#duration').val());
      setInterval(function(){
        $.ajax({
          type: "POST",
          url: "{{route('update_report_gst')}}",
          data: {
            'report_id' : $('#id_report').val(),
            "_token": "{{ csrf_token() }}"
          },
          success: function(data) {
            // alert(data);
          }
        });
        
      }, (Math.floor(duration/2)*1000));

      function strToSecond(txt) {
        var time = txt.split(":");
        var hour = 0;
        var minutes = 0;
        var second = 0;

        switch(time.length) {
          case 1:
            second = parseInt(time[0]);
            break;
          case 2:
            minutes = parseInt(time[0]);
            second = parseInt(time[1]);
            break;
          case 3:
            hour = parseInt(time[0]);
            minutes = parseInt(time[1]);
            second = parseInt(time[2]);
        }
        return (hour*3600) + (minutes*60) + second;
        
      }
     });
    </script>
@endsection
    
