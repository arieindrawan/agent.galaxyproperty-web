@extends('layouts.app')
@section('content')
<div class="site-section site-section-sm" style="background-color: #808080; margin-bottom: 1em;">

</div>
      <div class="site-section site-section-sm" style="background-color: transparent;">
        <div class="container">
          <div class="realestate-filter nav " style="background-color: transparent;">
            <a href="#" class="active" data-toggle="tab" id="agent-report" aria-controls="rent" aria-selected="true">Agent Report</a>
            <a href="#" class="" data-toggle="tab" id="vidio-report" aria-controls="sale" aria-selected="false">Vidio Report</a>
            <a href="#" class="" data-toggle="tab" id="statistic" aria-controls="sale" aria-selected="false">Statistic</a>
          </div>
          <div class="row" style="margin-top: 1em;">
            <div class="container">
                <div class="container  result">
                </div>
              </div>
          </div>
        </div>
      </div>

    
@endsection
@section('js_extras')
<script type="text/javascript">
  $(document).ready(function() {
    var dataPoints = [];
    $.ajax({
          url: "{{route('gst/getAgentReport')}}",
          success: function(data) {
            $(".result").html(data);
          }
        });
    $("#agent-report").click(function() {
        $.ajax({
          url: "{{route('gst/getAgentReport')}}",
          success: function(data) {
            $(".result").html(data);
          }
        });
    });
    $("#vidio-report").click(function() {
         $.ajax({
          url: "{{route('gst/getVidioReport')}}",
          success: function(data) {
            $(".result").html(data);
          }
        });
    });
    $("#statistic").click(function() {
      var str ="<div id='chartContainer' style='height: 370px; width: 100%;'></div>";
      $(".result").html(str);
                        
        var chart = new CanvasJS.Chart("chartContainer", {
          animationEnabled: true,
          theme: "light2",
          title: {
            text: "Daily Sales Data"
          },
          axisY: {
            title: "Units",
            titleFontSize: 24,
            includeZero: true
          },
          data: [{
            type: "column",
            yValueFormatString: "#,### Units",
            dataPoints: dataPoints
          }]
        });
        $.getJSON("https://canvasjs.com/data/gallery/javascript/daily-sales-data.json", addData);
    
        //  $.ajax({
        //   url: "{{route('gst/getStatistic')}}",
        //   success: function(data) {
        //     $(".result").html(data);
        //   }
        // });
    });
    function addData(data) {
      for (var i = 0; i < data.length; i++) {
        dataPoints.push({
          x: new Date(data[i].date),
          y: data[i].units
        });
      }
      chart.render();

    }
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
                window.open($(this).data("href"));
            });
        });
  function toogleDataSeries(e){
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
      e.dataSeries.visible = false;
    } else{
      e.dataSeries.visible = true;
    }
    e.chart.render();
  }
});
</script>
@endsection
    
