@extends('layouts.app')
@section('content')

<div class="site-section site-section-sm" style="background-color: #808080;">

</div>
<div class="row"  style="margin: 2em;">
    <div class="row" style="margin: 2em;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Vidio</h2>
            </div>
            <br>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('vidios.store') }}" method="POST" >
        @csrf

        <div class="row" style="margin: 2em;">
            <input id="id" class="form-control col-md-12 col-xs-12" name="id" type="hidden" value="{{@$vidio->id}}">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name" required="required" value="{{@$vidio->name}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Duration:</strong>
                    <input type="text" name="duration" class="form-control" placeholder="duration" required="required" value="{{@$vidio->duration}}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Url Link:</strong>
                    <textarea class="form-control" style="height:50px" name="link_url"
                        placeholder="link"></textarea>
                </div>
            </div>
            <input class="form-control col-md-12 col-xs-12" name="is_active" type="hidden" value="{{@$vidio->is_active}}">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
</div>
@endsection