@extends('layouts.app')
@section('content')
<div class="ftco-blocks-cover-1">
      <div class="site-section overlay site-section-sm" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
          </div>
        </div>
      </div>
    </div>
<div class="row" style="margin: 1em;">
    <div class="row" style="margin: 2em;">
        <p><a href="{{ route('vidios.create') }}" style="background-color: #c10016;" class="btn btn-primary text-white px-4 py-3">Add Vidio</a></p>
    </div>
    @if ($message = Session::get('success'))
    <div class="row" style="margin: 1em;" style="width: 100%;">
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    </div>   
        
    @endif
    <div class="row" style="margin: 2em; overflow-x:auto;">
    <table id="mytable" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Code</th>
                <th>Name</th>
                <th>Duration</th>
                <th>Link Url</th>
                <th width="20%">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($vidios as $vidio)
            <tr>
                <td>{{ $vidio->code}}</td>
                <td>{{ $vidio->name }}</td>
                <td>{{ $vidio->duration }}</td>
                <td><a target="blank" href="{{ $vidio->link_url }}">{{ $vidio->link_url }}</a></td>
                <td >
                    <form action="{{ route('vidios.destroy', $vidio->id) }}" method="POST">

                        <a href="{{ route('vidios.edit', $vidio->id) }}" title="show" class="btn btn-primary text-white px-2 py-1">
                            <i class="fa fa-edit"></i>
                        </a>

                        @csrf
                        @method('DELETE')
                        <button id="testform" type="submit" title="delete" class="btn btn-primary text-white px-2 py-1">
                            <i class="fa fa-trash-o"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
@endsection
@section('js_extras')
<script type="text/javascript">
$(document).ready(function(){
  $("#mytable").DataTable();

  $("#testform").click(function(event) {
      var hasil = confirm("Apakah anda yakin untuk menghapus vidio ini ?");
      if (!hasil) {
        event.preventDefault();
      }
    });
});
</script>
@endsection