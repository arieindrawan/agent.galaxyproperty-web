@extends('layouts.app')
@section('content')
<div class="ftco-blocks-cover-1">
      <div class="site-section overlay site-section-sm" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
          </div>
        </div>
      </div>
    </div>
<div class="row" style="margin: 1em;">
    @if ($message = Session::get('success'))
    <div class="row" style="margin: 1em;" style="width: 100%;">
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    </div>   
        
    @endif
    <div class="row" style="margin: 2em;">
        <p><a href="{{ route('wishlist/create') }}" style="background-color: #c10016;" class="btn btn-primary text-white px-4 py-3">Add Wishlist</a></p>
    </div>
    <div class="row" style="margin: 2em; overflow-x:auto;">
    <table id="mytable" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Title</th>
                <th>Transaction Type</th>
                <th>Property Category</th>
                <th>Province</th>
                <th>City</th>
                <th>Keyword</th>
                <th>Region</th>
                <th>Min price</th>
                <th>Max Price</th>
                <th>Bedroom</th>
                <th>Bathroom</th>
                <th width="20%">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($wishlists as $wishlist)
            <tr>
                <td>{{$wishlist->title}}</td>
                <td>{{$wishlist->type}}</td>
                <td>{{$wishlist->propertycategory_name}}</td>
                <td>{{$wishlist->province_name}}</td>
                <td>{{$wishlist->city_name}}</td>
                <td>{{$wishlist->keyword}}</td>
                <td>{{$wishlist->region}}</td>
                <td>{{$wishlist->min_price}}</td>
                <td>{{$wishlist->max_price}}</td>
                <td>{{$wishlist->bedroom}}</td>
                <td>{{$wishlist->bathroom}}</td>
                <td >
                    <a href="{{ route('wishlist/edit', $wishlist->id) }}" title="show" class="btn btn-primary text-white px-2 py-1">
                            <i class="fa fa-edit"></i>
                    </a>
                    <form action="{{ route('wishlist/destroy', $wishlist->id) }}" method="POST">
                        @csrf
                        <button id="testform" type="submit" title="delete" class="btn btn-primary text-white px-2 py-1">
                            <i class="fa fa-trash-o"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    </div>
</div>
@endsection
@section('js_extras')
<script type="text/javascript">
$(document).ready(function(){
  $("#mytable").DataTable();

  $("#testform").click(function(event) {
      var hasil = confirm("Apakah anda yakin untuk menghapus Wishlist ini ?");
      if (!hasil) {
        event.preventDefault();
      }
    });
});
</script>
@endsection