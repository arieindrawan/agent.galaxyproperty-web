@extends('layouts.app')
@section('content')

<div class="ftco-blocks-cover-1">
      <div class="site-section overlay site-section-sm" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
          </div>
        </div>
      </div>
    </div>
    <div class="row" style="margin: 3em;">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Wishlist</h2>
            </div>
            <br>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<form id="search-form" action="{{ route('wishlist/update') }}" method="POST">
    @csrf
      <div class="realestate-filter">
        <div class="container">
          <div class="realestate-filter-wrap nav">

            <input type="hidden" name="id" @if(!empty($param['transaction_type'])) value="{{$param['id']}}" @else value="" @endif id="id">
             <input type="hidden" name="type" @if(!empty($param['transaction_type'])) value="{{$param['transaction_type']}}" @else value="all" @endif id="type">
            <a href="#all" class="" data-toggle="tab" id="all-tab" aria-controls="all" aria-selected="false">All</a>
            <a href="#for-sell" class="" data-toggle="tab" id="sale-tab" aria-controls="sale" aria-selected="false">For Sell</a>
            <a href="#for-rent" class="" data-toggle="tab" id="rent-tab" aria-controls="rent" aria-selected="false">For Rent</a>
          </div>
        </div>
      </div>
      
      <div class="realestate-tabpane pb-5">
        <div class="container tab-content">
           <div class="tab-pane active" id="for-rent" role="tabpanel" aria-labelledby="rent-tab">

             <div class="row">
                <div class="col-md-4 form-group">
                 <input name="keyword" type="text" class="form-control" placeholder="Keyword" value="{{$param['keyword']}}">
               </div>
               <div class="col-md-4 form-group">
                 <select name="propertyCategory" id="propertyCategory" class="form-control w-100">
                   <option value="">All Types</option>
                   @foreach($propertyCategory as $val)
                   @php
                      $selected = "";
                      if(!empty($param['property_type']) && $param['property_type'] == $val->id){
                          $selected = "selected=selected";
                      }
                   @endphp
                   <option value="{{$val->id}}" {{$selected}}>{{$val->name}}</option>
                   @endforeach
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <input name="title" type="text" class="form-control" placeholder="Title" value="{{$param['title']}}">
               </div>
             </div>

             <div class="row">
               <div class="col-md-4 form-group">
                 <select name="province" id="province" class="form-control w-100">
                   <option value="">Province</option>
                    @foreach($province as $val)
                    @php
                      $selected = "";
                      if(!empty($param['province']) && $param['province'] == $val->id){
                          $selected = "selected=selected";
                      }
                   @endphp
                   <option value="{{$val->id}}" {{$selected}}>{{$val->name}}</option>
                   @endforeach
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <select name="city" id="city" class="form-control w-100">
                   <option value="">City</option>
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <input name="region" id="region" type="text" class="form-control" placeholder="Region" @if(!empty($param['region'])) value="{{$param['region']}}" @endif>
               </div>
             </div>

             <div class="row">
               <div class="col-md-4 form-group">
                 <select name="bed" id="bed" class="form-control w-100">
                  <option value="">Any Bedrooms</option>
                   <option value="0" <?php $selected = ""; (($param['bed']==0 && $param['bed']!="") ? $selected='selected' : $selected="") ?> {{$selected}}>0</option>
                   <option value="1" <?php $selected = ""; (($param['bed']==1)? $selected='selected' : $selected="") ?> {{$selected}}>1</option>
                   <option value="2" <?php $selected = ""; (($param['bed']==2)? $selected='selected' : $selected="") ?> {{$selected}}>2</option>
                   <option value="3" <?php $selected = ""; (($param['bed']==3)? $selected='selected' : $selected="") ?> {{$selected}}>3+</option>
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <select name="bath" id="bath" class="form-control w-100">
                   <option value="">Any Bathrooms</option>
                    <option value="0" <?php $selected = ""; (($param['bath']==0 && $param['bath']!="") ? $selected='selected' : $selected="") ?> {{$selected}}>0</option>
                   <option value="1" <?php $selected = ""; (($param['bath']==1)? $selected='selected' : $selected="") ?> {{$selected}}>1</option>
                   <option value="2" <?php $selected = ""; (($param['bath']==2)? $selected='selected' : $selected="") ?> {{$selected}}>2</option>
                   <option value="3" <?php $selected = ""; (($param['bath']==3)? $selected='selected' : $selected="") ?> {{$selected}}>3+</option>
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <div class="row">
                   <div class="col-md-6 form-group">
                     <select name="min_price" id="min_price" class="form-control w-100">
                       <option value="">Min Price</option>
                      <option value="100000000" <?php $selected = ""; (($param['min_p']==100000000)? $selected='selected' : $selected="") ?> {{$selected}}>100.000.000</option>
                       <option value="500000000" <?php $selected = ""; (($param['min_p']==500000000)? $selected='selected' : $selected="") ?> {{$selected}}>500.000.000</option>
                       <option value="1000000000" <?php $selected = ""; (($param['min_p']==1000000000)? $selected='selected' : $selected="") ?> {{$selected}}>1.000.000.000</option>
                       <option value="5000000000" <?php $selected = ""; (($param['min_p']==5000000000)? $selected='selected' : $selected="") ?> {{$selected}}>5.000.000.000</option>
                       <option value="10000000000" <?php $selected = ""; (($param['min_p']==10000000000)? $selected='selected' : $selected="") ?> {{$selected}}>10.000.000.000</option>
                     </select>
                   </div>
                   <div class="col-md-6">
                     <select name="max_price" id="max_price" class="form-control w-100">
                       <option value="">Max Price</option>
                      <option value="100000000" <?php $selected = ""; (($param['max_p']==100000000)? $selected='selected' : $selected="") ?> {{$selected}}>100.000.000</option>
                       <option value="500000000" <?php $selected = ""; (($param['max_p']==500000000)? $selected='selected' : $selected="") ?> {{$selected}}>500.000.000</option>
                       <option value="1000000000" <?php $selected = ""; (($param['max_p']==1000000000)? $selected='selected' : $selected="") ?> {{$selected}}>1.000.000.000</option>
                       <option value="5000000000" <?php $selected = ""; (($param['max_p']==5000000000)? $selected='selected' : $selected="") ?> {{$selected}}>5.000.000.000</option>
                       <option value="10000000000" <?php $selected = ""; (($param['max_p']==10000000000)? $selected='selected' : $selected="") ?> {{$selected}}>10.000.000.000</option>
                     </select>
                   </div>
                 </div>
               </div>
             </div>
             <div class="row">
               <div class="col-md-4">
                 <input type="submit" id="btn-save" class="btn btn-black py-3 btn-block" value="Save">
               </div>
             </div>

           </div>
           </div>
        </div>
      </div>
    </form>
</div>
@endsection
@section('js_extras')
<script type="text/javascript">
$(document).ready(function() {
  var province_id = $("#province").val();
  getCity(province_id);
  switch($('#type').val()) {
    case "all":
      $("#all-tab").attr("class", "active");
      break;
    case "sell":
       $("#sale-tab").attr("class", "active");
      break;
    case "rent":
      $("#rent-tab").attr("class", "active");
      break;
  }

  $("#all-tab").click(function() {
        $('#type').val('all');
    });
  $("#rent-tab").click(function() {
        $('#type').val('rent');
    });
  $("#sale-tab").click(function() {
        $('#type').val('sell');
    });
  $("#province").change(function() {
    var province_id = $("#province").val();
    getCity(province_id);
  });
  $("#btn-save").on("click", function(e){
      var is_required = true;
      if ($('#min_price').val() =="") {
        is_required = false;
        alert('Minimal Price harus diisi!');
      }
      if ($('#max_price').val()  =="") {
        is_required = false;
        alert('Maximal Price harus diisi!');
      }
      if ($('#keyword').val()=="") {
        is_required = false;
        alert('Keyword Price harus diisi!');
      }
      if ($('#propertyCategory').val()=="") {
        is_required = false;
        alert('Property Type harus diisi!');
      }
      if (is_required == false) {
          alert("Data Harus Diisi Lenhgkap!");
          event.preventDefault();
      }   
  });
});
</script>
@endsection