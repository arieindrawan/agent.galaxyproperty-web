<header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">
            <div class="col-3 ">
                <img src="{{asset('images/galaxy_logo_red.png')}}" style="width: 130px; height: 40px;">
            </div>

            <div class="col-9  text-right">
              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                  <li><a href="{{ route('home')}}" class="nav-link">Home</a></li>
                  <li><a href="{{ route('property')}}" class="nav-link">Property</a></li>
                  @if (Auth::check())
                  @else
                  <li><a href="{{ route('open-house')}}" class="nav-link">Open House</a></li>
                  @endif
                  @if (Auth::check())
                  <li><a href="{{ route('wishlist')}}" class="nav-link">My Wishlist</a></li>
                  @if(Auth::user()->is_admin)
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      GST Manager
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style=" background-color: white;">
                      <a class="dropdown-item" href="{{ route('gst')}}" class="nav-link">GST</a>
                      <a class="dropdown-item" href="{{ route('vidios.index')}}">List Vidio</a>
                      <a class="dropdown-item" href="{{ route('gst/report')}}">Report GST</a>
                    </div>
                  </li>
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Open House Manager
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style=" background-color: white;">
                      <a href="{{ route('open-house')}}" class="dropdown-item">Open House</a>
                      <a href="{{ route('open-house/create')}}" class="dropdown-item">List All Property</a>
                      <a href="{{ route('open-house/list-property')}}" class="dropdown-item">List Property Open House</a>
                      <a href="{{ route('open-house/cetak-listing')}}" class="dropdown-item">Cetak Listing</a>
                    </div>
                  </li>
                  @else
                  <li><a href="{{ route('gst')}}" class="nav-link">GST</a></li>
                  @endif
                  <li>
                    <li>
                    <form method="POST" action="{{ route('logout') }}">
                              @csrf

                              <x-jet-dropdown-link style="color: #c10016;" href="{{ route('logout') }}"
                                                      onclick="event.preventDefault();
                                                                  this.closest('form').submit();">
                                      {{ __('Logout') }}
                                  </x-jet-dropdown-link>
                              </form>
                  </li>
                  @else
                  <li><a href="{{ route('login') }}" class="nav-link">Login</a></li>
                  @endif
                  
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>