<header class="site-navbar site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">

            <div class="col-3 ">
                <img src="{{asset('images/galaxy_logo_red.png')}}" style="width: 150px; height: 50px;">
            </div>

            <div class="col-9  text-right">
              

              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>

              

              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                  <li><a href="{{ route('home')}}" class="nav-link">Home</a></li>
                  @if (Auth::check())
                  <li><a href="{{ route('gst')}}" class="nav-link">Gst</a></li>
                  @if(Auth::user()->is_admin)
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     GST Manager
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="opacity: 0.5; background-color: #ffffff;">
                      <a class="dropdown-item" href="{{ route('vidios.index')}}">List Vidio</a>
                      <a class="dropdown-item" href="{{ route('gst/report')}}">Report GST</a>
                    </div>
                  </li>
                  @endif

                  @endif
                  <li><a href="{{ route('property')}}" class="nav-link">Property</a></li>
                  
                  @if (Auth::check())
                  <li><a href="{{ route('wishlist.index')}}" class="nav-link">My Wishlist</a></li>
                  <li>
                   <nav class="navbar navbar-expand-sm"> 
                      <div class="collapse navbar-collapse" id="navbar-list-4">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <img src="https://s3.eu-central-1.amazonaws.com/bootstrapbaymisc/blog/24_days_bootstrap/fox.jpg" width="40" height="40" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style="opacity: 0.5; background-color: #ffffff;">
                              <a class="dropdown-item" href="{{ route('profile.show') }}">Profil</a>
                              <a class="dropdown-item" href="#">Edit Profile</a>

                              <form method="POST" action="{{ route('logout') }}">
                              @csrf

                              <x-jet-dropdown-link href="{{ route('logout') }}"
                                                      onclick="event.preventDefault();
                                                                  this.closest('form').submit();">
                                      {{ __('Logout') }}
                                  </x-jet-dropdown-link>
                              </form>
                            </div>
                          </li>   
                        </ul>
                      </div>
                    </nav>
                  </li>
                  @else
                  <li><a href="{{ route('login') }}" class="nav-link">Login</a></li>
                  @endif
                  
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

      </header>