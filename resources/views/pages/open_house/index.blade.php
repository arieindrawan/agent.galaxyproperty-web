@extends('layouts.app')
@section('content')
<div class="ftco-blocks-cover-1">
    <div class="site-section-cover" data-stellar-background-ratio="1" style="background-image: url('images/WEB-BANNER-01-01.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-7">
              <h1 class="mb-2"></h1>
              <p class="text-white"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section bg-black">
      <div class="container">
        <div class="row">
          <?php 
            $jakarta = "DKI Jakarta";
            $sby = "Surabaya";
            $malang = "Malang";
            $bali = "Bali";
           ?>
            <div class="col-md-6 mb-5">
              <div class="media-38289">
                <a href="{{route('open-house/city',$jakarta)}}" class="d-block"><img src="{{asset('images')}}/jakarta.jpg" alt="Image" class="img-fluid"></a>
                <div class="text" align="center" style="background-color: #c10016">
                  <h3 class="mb-3"><a href="{{route('open-house/city',$jakarta)}}" style="color: white">JAKARTA</a></h3>
                </div>
              </div>
            </div>

            <div class="col-md-6 mb-5">
              <div class="media-38289">
                <a href="{{route('open-house/city',$sby)}}" class="d-block"><img src="{{asset('images')}}/surabaya.jpg" alt="Image" class="img-fluid"></a>
                <div class="text" align="center" style="background-color: #c10016">
                  <h3 class="mb-3"><a href="{{route('open-house/city',$sby)}}" style="color: white">SURABAYA</a></h3>
                </div>
              </div>
            </div>

            <div class="col-md-6 mb-5">
              <div class="media-38289">
                <a href="{{route('open-house/city',$bali)}}" class="d-block"><img src="{{asset('images')}}/bali.jpg" alt="Image" class="img-fluid"></a>
                <div class="text" align="center" style="background-color: #c10016">
                  <h3 class="mb-3"><a href="{{route('open-house/city',$bali)}}" style="color: white">BALI</a></h3>
                </div>
              </div>
            </div>

            <div class="col-md-6 mb-5">
              <div class="media-38289">
                <a href="{{route('open-house/city',$malang)}}" class="d-block"><img src="{{asset('images')}}/malang.jpg" alt="Image" class="img-fluid"></a>
                <div class="text" align="center" style="background-color: #c10016">
                  <h3 class="mb-3"><a href="{{route('open-house/city',$malang)}}" style="color: white">MALANG</a></h3>
                </div>
              </div>
            </div>

        </div>
      </div>
    </div>
@endsection
@section('js_extras')
<script type="text/javascript">

</script>
@endsection
