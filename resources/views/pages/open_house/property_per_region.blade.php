@extends('layouts.app')
@section('content')
<div class="ftco-blocks-cover-1">
    <div class="site-section-cover" data-stellar-background-ratio="1" style="background-image: url('http://agent.propertilaris.com/public/images/WEB-BANNER-01-01.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-7">
              <h1 class="mb-2"></h1>
              <p class="text-white"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section bg-black">
      <div class="container">
        <div class="row">
          @foreach ($property as $val)
            <div class="col-md-4 mb-5">
              <div class="media-38289">
                <a href="https://galaxyproperty.co.id/property/detail/{{$val->number}}" class="d-block" target="blank"><img src="@if($val->path_images!=null)https://galaxyproperty.co.id/public/{{$val->path_images['small']}} @else {{asset('images')}}/default_image-1.jpg @endif" alt="Image" class="img-fluid"></a>
                <div class="text">
                  <div class="d-flex justify-content-between mb-3">
                    <div class="sq d-flex align-items-center"><span class="wrap-icon icon-fullscreen"></span> <span>{{$val->title}}</span></div>
                    <div class="bed d-flex align-items-center"><span class="wrap-icon icon-bed"></span> <span>{{$val->bedroom}}</span></div>
                    <div class="bath d-flex align-items-center"><span class="wrap-icon icon-bath"></span> <span>{{$val->bathroom}}</span></div>
                  </div>
                  <h3 class="mb-3"><a href="https://galaxyproperty.co.id/property/detail/{{$val->number}}" target="blank" style="color: #c10016">Rp. {{substr( number_format($val->price,2,',','.'),0,-3)}}</a></h3>
                  <span class="d-block small address d-flex align-items-center"> <span class="icon-room mr-3 text-primary"></span> <span>{{$val->address}}</span></span>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="row align-items-center justify-content-center  text-center pagination-39291" style="margin: auto; position: relative; text-align: center">
            <div class="pagination-39291">
              {{ $property->withQueryString()->links('vendor.pagination.bootstrap-4') }}
            </div>
              
        </div>
      </div>
    </div>
@endsection
