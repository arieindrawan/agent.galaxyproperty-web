<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <!--[if gte mso 9]><xml>
  <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
  </xml><![endif]-->
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="format-detection" content="date=no" />
  <meta name="format-detection" content="address=no" />
  <meta name="format-detection" content="telephone=no" />
  <title>PDF Listing Template</title>
  

  <style type="text/css" media="screen">
    /* Linked Styles */
    @font-face {
        font-family: 'Poppins';
        font-style: normal;
        font-weight: normal;
        src: url('http://agent.propertilaris.com/public/fonts/Poppins-Medium.ttf') format('truetype');
    }
    body { padding:0 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none }
    a { color:#a88123; text-decoration:none }
    p { padding:0 !important; margin:0 !important; font-family: 'Poppins'; letter-spacing: normal; font-variant-ligatures: 0; } 

    @page {
        size: a4 potrait; 
        margin:0;

      } 
      body {
        border: thin solid black;
      }
    /* Mobile styles */
    </style>
    <style media="only screen and (max-device-width: 480px), only screen and (max-width: 480px)" type="text/css">
    @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) { 
      div[class='mobile-br-5'] { height: 5px !important; }
      div[class='mobile-br-10'] { height: 10px !important; }
      div[class='mobile-br-15'] { height: 15px !important; }
      div[class='mobile-br-20'] { height: 20px !important; }
      div[class='mobile-br-25'] { height: 25px !important; }
      div[class='mobile-br-30'] { height: 30px !important; }

      th[class='m-td'], 
      td[class='m-td'], 
      div[class='hide-for-mobile'], 
      span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

      span[class='mobile-block'] { display: block !important; }

      div[class='wgmail'] img { min-width: 320px !important; width: 320px !important; }

      div[class='img-m-center'] { text-align: center !important; }

      div[class='fluid-img'] img,
      td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }

      table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
      td[class='td'] { width: 100% !important; min-width: 100% !important; }
      
      table[class='center'] { margin: 0 auto; }
      
      td[class='column-top'],
      th[class='column-top'],
      td[class='column'],
      th[class='column'] { float: left !important; width: 100% !important; display: block !important; }

      td[class='content-spacing'] { width: 15px !important; }

      div[class='h2'] { font-size: 44px !important; line-height: 48px !important; }
    } 
  </style>
</head>
<body class="body" style="padding:0px; !important; margin:0px; !important; display:block !important; background:grey; -webkit-text-size-adjust:none">
    <div  style="background-image: url('http://agent.propertilaris.com/public/images/flyer-final-02.jpg'); object-fit: cover; width: 100%; height: 100%; background-size: 100% 100%; background-repeat: no-repeat; ">
      <div class="row" style="height: 230px;">

      </div>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style=" padding-left: 15px; padding-right: 15px; letter-spacing: normal;">
        @foreach($property as $key => $val)
        <tr style="margin: 15px; padding-left: 15px; padding-right: 15px; background-color: #F5F5F5;">
          <td>
            <table style="table-layout: fixed; width: 100%;">
              <tr>
                <td style="width: 50%;">
                  <div class="row" style="margin: 5px;">
                    @if($val->path_images!=null)
                    <img src="https://galaxyproperty.co.id/public/{{$val->path_images['large']}}" height="250" width="360">
                    @else
                    <img src="https://galaxyproperty.co.id/public/images/Setting/41/default_image-1.jpg" height="250" width="360">
                    @endif
                    
                  </div>
                </td>
                <td style="width: 50%;">
                  <table  style="width: 100%;">
                    <tr>
                      <td colspan="2" style="text-transform:uppercase; font-size: 18pt; padding-bottom: 0px; margin-bottom: 0px; line-height: 5px;">
                        <p>FOR {{$val->transaction_type}}</p>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" style="line-height: 15px; color: #c10016; font-size: 18pt;">
                        <p>{{$val->title}}</p>
                      </td>
                    </tr>
                    <tr>
                      <td height="30px">
                        
                      </td>
                    </tr>
                    <tr>
                      <td align="left"  style="width: 50%;">
                        <p>LT: {{$val->land_area}}</p>
                      </td>
                      <td align="right"  style="width: 50%; padding-right: 10px;">
                        <p>{{$val->bedroom}} Kamar Tidur</p>
                      </td>
                    </tr>
                    <tr>
                      <td align="left"  style="width: 50%;">
                        <p>LB: {{$val->building_area}}</p>
                      </td>
                      <td align="right"  style="width: 50%; padding-right: 10px;">
                        <p>{{$val->bathroom}} Kamar Mandi</p>
                      </td>
                    </tr>
                    <tr>
                      <td style="width: 100%; padding-right: 10px;" colspan="2">
                        <hr>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2" style="line-height: 10px; font-size: 18pt; color: #c10016;"><p>IDR {{substr( number_format($val->price,2,',','.'),0,-3)}}</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <div class="row" style="height: 20px;">
            
            </div>    
          </td>
        </tr>
        @if(($key+1)%3==0)
        @if($key!=(count($property)-1))
        <tr>
          <td>
            <div class="row" style="height: 230px;">
            
            </div>    
          </td>
        </tr>
        @endif
        @endif
        @endforeach
      </table>
                           
    </div>