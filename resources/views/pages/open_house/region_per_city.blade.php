@extends('layouts.app')
@section('content')
<div class="ftco-blocks-cover-1">
    <div class="site-section-cover" data-stellar-background-ratio="1" style="background-image: url('http://agent.propertilaris.com/public/images/WEB-BANNER-01-01.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-7">
              <h1 class="mb-2"></h1>
              <p class="text-white"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section bg-black">
      <div class="container">
        <div class="row">
            @foreach($region as $val)
            <div class="col-md-6 mb-5">
              <div class="media-38289">
                <div class="text" align="center" style="background-color: #c10016">
                  <h3 class="mb-3"><a href="{{route('open-house-galaxy',$val->region)}}" style="color: white">{{$val->region}}</a></h3>
                </div>
              </div>
            </div>
            @endforeach
        </div>
      </div>
    </div>
@endsection
@section('js_extras')
<script type="text/javascript">

</script>
@endsection
