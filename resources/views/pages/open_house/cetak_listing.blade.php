@extends('layouts.app')
@section('content')

<div class="ftco-blocks-cover-1">
      <div class="site-section overlay site-section-sm" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
          </div>
        </div>
      </div>
    </div>
<div class="row" style="margin: 1em;">
    @if ($message = Session::get('success'))
    <div class="row" style="margin: 1em;" style="width: 100%;">
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    </div>   
        
    @endif
    <div class="row" style="margin: 2em; overflow-x:auto;">
    <table id="mytable" class="table table-striped table-hover">
        <thead>
            <tr>
                <th>No.</th>
                <th>Title</th>
                <th>Region</th>
                <th>Harga</th>
                <th>Image</th>
                <th width="20%">Print</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($property as $key => $val)
            <tr data-href="https://galaxyproperty.co.id/property/detail/{{$val->number}}">
                <td>{{$key+1}}</td>
                <td>{{$val->title}}</td>
                <td>{{$val->region}}</td>
                <td>Rp. {{substr( number_format($val->price,2,',','.'),0,-3)}}</td>
                <td id="vidio" width="20%" height="130px" class="align-middle">
                                <img width="100%" height="100%" src="@if($val->path_images!=null)https://galaxyproperty.co.id/public/{{$val->path_images['small']}} @else {{asset('images')}}/default_image-1.jpg @endif">
                                  
                </td>
                <td align="center" width="20%" height="130px" class="align-middle">
                    <input style="height: 40px; height: 40px;" onclick="addListing({{$val->id}})" type="checkbox" class="editor-active" id="check-print" value="{{$val->id}}">              
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="site-section-sm bg-white" style="margin-bottom: 5em;">
      <div class="container">
        <div class="row" style="margin-right: 4em; margin-top: 1em; position: absolute;   right: 0px;">
        
          <a style="background-color: #c10016;" href="#submit" id="btn-export" class="btn btn-primary text-white px-4 py-3">Cetak Listing</a>
          
        </div>
      </div>
    </div>
    </div>
</div>
@endsection
@section('js_extras')
<script type="text/javascript">
var arrListing = [];
$(document).ready(function(){
  $("#mytable").DataTable();
  $(".clickable-row").click(function() {
                window.open($(this).data("href"));
            });
  $("#testform").click(function(event) {
      var hasil = confirm("Apakah anda yakin untuk menghapus dari list Property Open House ?");
      if (!hasil) {
        event.preventDefault();
      }
    });
    $("#btn-export").on("click", function(e){
        e.preventDefault();
        if (arrListing.length > 0) {

            var url = "{{ route('open-house/export', ':id') }}";
            url = url.replace(':id',JSON.stringify(arrListing));

            //window.location.href = url;
            window.location.replace(url);


        }
        else{
            alert("Silahkan pilih property yang ingin dicetak.");
        }
    });
});
function addListing(id){
    if (arrListing.includes(id)) {
        var idx = arrListing.indexOf(id);
        arrListing.splice(idx, 1);
    }
    else{
        arrListing.push(id);
    }
}
</script>
@endsection