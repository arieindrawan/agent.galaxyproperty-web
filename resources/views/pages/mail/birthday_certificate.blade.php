<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <!--[if gte mso 9]><xml>
  <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
  </xml><![endif]-->
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="format-detection" content="date=no" />
  <meta name="format-detection" content="address=no" />
  <meta name="format-detection" content="telephone=no" />
  <title>Email Template</title>
  

  <style type="text/css" media="screen">
    /* Linked Styles */
    body { padding:1 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none }
    a { color:#a88123; text-decoration:none }
    p { padding:1 !important; margin:0 !important } 

    /* Mobile styles */
    </style>
    <style media="only screen and (max-device-width: 480px), only screen and (max-width: 480px)" type="text/css">
    @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) { 
      div[class='mobile-br-5'] { height: 5px !important; }
      div[class='mobile-br-10'] { height: 10px !important; }
      div[class='mobile-br-15'] { height: 15px !important; }
      div[class='mobile-br-20'] { height: 20px !important; }
      div[class='mobile-br-25'] { height: 25px !important; }
      div[class='mobile-br-30'] { height: 30px !important; }

      th[class='m-td'], 
      td[class='m-td'], 
      div[class='hide-for-mobile'], 
      span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

      span[class='mobile-block'] { display: block !important; }

      div[class='wgmail'] img { min-width: 320px !important; width: 320px !important; }

      div[class='img-m-center'] { text-align: center !important; }

      div[class='fluid-img'] img,
      td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }

      table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
      td[class='td'] { width: 100% !important; min-width: 100% !important; }
      
      table[class='center'] { margin: 0 auto; }
      
      td[class='column-top'],
      th[class='column-top'],
      td[class='column'],
      th[class='column'] { float: left !important; width: 100% !important; display: block !important; }

      td[class='content-spacing'] { width: 15px !important; }

      div[class='h2'] { font-size: 44px !important; line-height: 48px !important; }
    } 
  </style>
</head>
<body class="body" style="padding:1 !important; margin:0 !important; display:block !important; background:white; -webkit-text-size-adjust:none">
    <div  style="background-image: url('http://agent.propertilaris.com/public/images/galaxy-birthday-template-01.jpg'); width: 100%; height: 100%;">
      <table width="100%" border="0" cellspacing="0" cellpadding="1">
                            <tr>
                              <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
                              <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="70" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:40pt; line-height:26px; text-align:center; margin: 40px;"><b>happy</b></div>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:40pt; line-height:26px; text-align:center"><b>birthday!</b></div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="70" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:10pt; line-height:26px; text-align:center">OUR DEAREST,</div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="10" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:23pt; line-height:26px; text-align:center">{{strtoupper($data_agent->name)}}</div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="5" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:10pt; line-height:26px; text-align:center">GALAXY </div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="70" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:9pt; line-height:26px; text-align:center">WE ARE WISHING YOU AN AMAZING BIRTHDAY AND</div>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:9pt; line-height:26px; text-align:center">A GREAT YEAR AHEAD FILLED WITH BLESSING,</div>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:9pt; line-height:26px; text-align:center">GOOD HEALTH, WISDOM, AND HAPPINESS!</div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="1" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="70" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                                <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:15pt; line-height:26px; text-align:center">Best Regards,</div>
                                 <div class="h3-1-center" style="color:white; font-family:Georgia, serif; min-width:auto !important; font-size:15pt; line-height:26px; text-align:center">
                                   <img style="width: 50%;" src="http://agent.propertilaris.com/public/images/logo-white-b.png">
                                 </div>
                                 <table width="100%" border="0" cellspacing="0" cellpadding="1" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="70" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                              </td>
                            </tr>
                          </table>
    </div>