<form id="search-form" action="{{ route('property') }}" method="GET">
      <div class="realestate-filter">
        <div class="container">
          <div class="realestate-filter-wrap nav">

            <input type="hidden" name="type" @if(!empty($param['transaction_type'])) value="{{$param['transaction_type']}}" @else value="all" @endif id="type">
            <a href="#all" class="" data-toggle="tab" id="all-tab" aria-controls="all" aria-selected="false">All</a>
            <a href="#for-sell" class="" data-toggle="tab" id="sale-tab" aria-controls="sale" aria-selected="false">For Sell</a>
            <a href="#for-rent" class="" data-toggle="tab" id="rent-tab" aria-controls="rent" aria-selected="false">For Rent</a>
          </div>
        </div>
      </div>
      
      <div class="realestate-tabpane pb-5">
        <div class="container tab-content">
           <div class="tab-pane active" id="for-rent" role="tabpanel" aria-labelledby="rent-tab">
<!--               <div class="row">
               <div class="col-md-12 form-group">
                  <div class="radio-inline text-white px-4 py-3">
                    <label><input id="all-tab" type="radio" name="optradio" checked>All</label>
                    <label><input id="sale-tab" type="radio" name="optradio">For Sell</label>
                    <label><input id="rent-tab" type="radio" name="optradio">For Rent</label>
                  </div>
               </div>
             </div> -->

             <div class="row">
              <div class="col-md-4 form-group">
                 <input name="keyword" id="keyword" type="text" class="form-control" placeholder="keyword" value="{{$param['keyword']}}">
               </div>
               <div class="col-md-4 form-group">
                 <select name="propertyCategory" id="propertyCategory" class="form-control w-100">
                   <option value="">All Types</option>
                   @foreach($propertyCategory as $val)
                   @php
                      $selected = "";
                      if(!empty($param['property_type']) && $param['property_type'] == $val->id){
                          $selected = "selected=selected";
                      }
                   @endphp
                   <option value="{{$val->id}}" {{$selected}}>{{$val->name}}</option>
                   @endforeach
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <input name="title" id="title" type="text" class="form-control" placeholder="Title" value="{{$param['title']}}">
               </div>
             </div>

             <div class="row">
               <div class="col-md-4 form-group">
                 <select name="province" id="province" class="form-control w-100">
                   <option value="">Province</option>
                    @foreach($province as $val)
                    @php
                      $selected = "";
                      if(!empty($param['province']) && $param['province'] == $val->id){
                          $selected = "selected=selected";
                      }
                   @endphp
                   <option value="{{$val->id}}" {{$selected}}>{{$val->name}}</option>
                   @endforeach
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <select name="city" id="city" class="form-control w-100">
                   <option value="">City</option>
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <input name="region" id="region" type="text" class="form-control" placeholder="Region" @if(!empty($param['region'])) value="{{$param['region']}}" @endif>
               </div>
             </div>

             <div class="row">
               <div class="col-md-4 form-group">
                 <select name="bed" id="bed" class="form-control w-100">
                   <option value="">Any Bedrooms</option>
                   <option value="0" <?php $selected = ""; (($param['bed']==0 && $param['bed']!="") ? $selected='selected' : $selected="") ?> {{$selected}}>0</option>
                   <option value="1" <?php $selected = ""; (($param['bed']==1)? $selected='selected' : $selected="") ?> {{$selected}}>1</option>
                   <option value="2" <?php $selected = ""; (($param['bed']==2)? $selected='selected' : $selected="") ?> {{$selected}}>2</option>
                   <option value="3" <?php $selected = ""; (($param['bed']==3)? $selected='selected' : $selected="") ?> {{$selected}}>3+</option>
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <select name="bath" id="bath" class="form-control w-100">
                   <option value="">Any Bathrooms</option>
                   <option value="0" <?php $selected = ""; (($param['bath']==0 && $param['bath']!="") ? $selected='selected' : $selected="") ?> {{$selected}}>0</option>
                   <option value="1" <?php $selected = ""; (($param['bath']==1)? $selected='selected' : $selected="") ?> {{$selected}}>1</option>
                   <option value="2" <?php $selected = ""; (($param['bath']==2)? $selected='selected' : $selected="") ?> {{$selected}}>2</option>
                   <option value="3" <?php $selected = ""; (($param['bath']==3)? $selected='selected' : $selected="") ?> {{$selected}}>3+</option>
                 </select>
               </div>
               <div class="col-md-4 form-group">
                 <div class="row">
                   <div class="col-md-6 form-group">
                     <select name="min_price" id="min_price" class="form-control w-100">
                       <option value="">Min Price</option>
                       <option value="100000000" <?php $selected = ""; (($param['min_p']==100000000)? $selected='selected' : $selected="") ?> {{$selected}}>100.000.000</option>
                       <option value="500000000" <?php $selected = ""; (($param['min_p']==500000000)? $selected='selected' : $selected="") ?> {{$selected}}>500.000.000</option>
                       <option value="1000000000" <?php $selected = ""; (($param['min_p']==1000000000)? $selected='selected' : $selected="") ?> {{$selected}}>1.000.000.000</option>
                       <option value="5000000000" <?php $selected = ""; (($param['min_p']==5000000000)? $selected='selected' : $selected="") ?> {{$selected}}>5.000.000.000</option>
                       <option value="10000000000" <?php $selected = ""; (($param['min_p']==10000000000)? $selected='selected' : $selected="") ?> {{$selected}}>10.000.000.000</option>
                     </select>
                   </div>
                   <div class="col-md-6">
                     <select name="max_price" id="max_price" class="form-control w-100">
                       <option value="">Max Price</option>
                       <option value="100000000" <?php $selected = ""; (($param['max_p']==100000000)? $selected='selected' : $selected="") ?> {{$selected}}>100.000.000</option>
                       <option value="500000000" <?php $selected = ""; (($param['max_p']==500000000)? $selected='selected' : $selected="") ?> {{$selected}}>500.000.000</option>
                       <option value="1000000000" <?php $selected = ""; (($param['max_p']==1000000000)? $selected='selected' : $selected="") ?> {{$selected}}>1.000.000.000</option>
                       <option value="5000000000" <?php $selected = ""; (($param['max_p']==5000000000)? $selected='selected' : $selected="") ?> {{$selected}}>5.000.000.000</option>
                       <option value="10000000000" <?php $selected = ""; (($param['max_p']==10000000000)? $selected='selected' : $selected="") ?> {{$selected}}>10.000.000.000</option>
                     </select>
                   </div>
                 </div>
               </div>
             </div>
             <div class="row">
               <div class="col-md-4">
                 <input type="submit" class="btn btn-black py-3 btn-block" value="Search">
               </div>
               @if (Auth::check())
               @if($is_search)
               <div class="col-md-8">
<!--                 <div class="col-md-4">
                  <input type="checkbox" id="is_wishlist" name="is_wishlist" value="1">
                <label for="add" class="text-white"> Add To Wishlist</label>
                <label title="Bila anda menambahkan data pencarian anda ke Wishlist, anda akan mendapatkan email notifikasi jika property yang anda cari tersedia." class="text-white px-2 py-1">
                            <i class="far fa-question-circle"></i>
                        </label>
                </div> -->
                <div class="col-md-4">
                 <a href="#submit" class="btn btn-black text-white px-4 py-3" id="btnAddWishlist">Add to Wishlist</a>
               </div>
               </div>
               
               @endif
                @endif
             </div>

           </div>
           </div>
        </div>
      </div>
    </form>