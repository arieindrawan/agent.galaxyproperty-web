@extends('layouts.app')
@section('content')
<div class="modal fade" id="modalWishlist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add to Wishlist</h5>
            </div>
            <div class="modal-body">
                  <form id="search-form-modal" action="{{ route('wishlist/store') }}" method="GET" style="margin-top: 2em;">
                    @csrf
                      <div class="realestate-filter" >
                        <div class="container">
                          <div class="realestate-filter-wrap nav">
                            <input type="hidden" name="typem" @if(!empty($param['transaction_type'])) value="{{$param['transaction_type']}}" @else value="all" @endif id="typem">
                            <a href="#all" class="" data-toggle="tab" id="all-tabm" aria-controls="all" aria-selected="false">All</a>
                            <a href="#for-sell" class="" data-toggle="tab" id="sale-tabm" aria-controls="sale" aria-selected="false">For Sell</a>
                            <a href="#for-rent" class="" data-toggle="tab" id="rent-tabm" aria-controls="rent" aria-selected="false">For Rent</a>
                          </div>
                        </div>
                      </div>
                      
                      <div class="realestate-tabpane pb-5">
                        <div class="container tab-content">
                           <div class="tab-pane active" id="for-rent" role="tabpanel" aria-labelledby="rent-tab">

                             <div class="row">
                                <div class="col-md-12 form-group">
                                 <input name="keywordm" id="keywordm" type="text" class="form-control" placeholder="keyword*" value="" required>
                               </div>
                               <div class="col-md-12 form-group">
                                 <select name="propertyCategorym" id="propertyCategorym" class="form-control w-100">
                                   <option value="">All Types *</option>
                                   @foreach($propertyCategory as $val)
                                   @php
                                      $selected = "";
                                      if(!empty($param['property_type']) && $param['property_type'] == $val->id){
                                          $selected = "selected=selected";
                                      }
                                   @endphp
                                   <option value="{{$val->id}}">{{$val->name}}</option>
                                   @endforeach
                                 </select>
                               </div>
                               <div class="col-md-12 form-group">
                                 <input name="titlem" id="titlem" type="text" class="form-control" placeholder="Title" value="">
                               </div>
                             </div>

                             <div class="row">
                               <div class="col-md-12 form-group">
                                 <select name="provincem" id="provincem" class="form-control w-100">
                                   <option value="">Province</option>
                                    @foreach($province as $val)
                                   @php
                                      $selected = "";
                                      if(!empty($param['province']) && $param['province'] == $val->id){
                                          $selected = "selected=selected";
                                      }
                                   @endphp
                                   <option value="{{$val->id}}">{{$val->name}}</option>
                                   @endforeach
                                 </select>
                               </div>
                               <div class="col-md-12 form-group">
                                 <select name="citym" id="citym" class="form-control w-100">
                                   <option value="">City</option>
                                 </select>
                               </div>
                               <div class="col-md-12 form-group">
                                 <input name="regionm" id="regionm" type="text" class="form-control" placeholder="Region"value="">
                               </div>
                             </div>

                             <div class="row">
                               <div class="col-md-12 form-group">
                                 <select name="bedm" id="bedm" class="form-control w-100">
                                   <option value="">Any Bedrooms</option>
                                   <option value="0" <?php $selected = ""; (($param['bed']==0 && $param['bed']!="") ? $selected='selected' : $selected="") ?> {{$selected}}>0</option>
                                   <option value="1" <?php $selected = ""; (($param['bed']==1)? $selected='selected' : $selected="") ?> {{$selected}}>1</option>
                                   <option value="2" <?php $selected = ""; (($param['bed']==2)? $selected='selected' : $selected="") ?> {{$selected}}>2</option>
                                   <option value="3" <?php $selected = ""; (($param['bed']==3)? $selected='selected' : $selected="") ?> {{$selected}}>3+</option>
                                 </select>
                               </div>
                               <div class="col-md-12 form-group">
                                 <select name="bathm" id="bathm" class="form-control w-100">
                                   <option value="">Any Bathrooms</option>
                                   <option value="0" <?php $selected = ""; (($param['bath']==0 && $param['bath']!="") ? $selected='selected' : $selected="") ?> {{$selected}}>0</option>
                                   <option value="1" <?php $selected = ""; (($param['bath']==1)? $selected='selected' : $selected="") ?> {{$selected}}>1</option>
                                   <option value="2" <?php $selected = ""; (($param['bath']==2)? $selected='selected' : $selected="") ?> {{$selected}}>2</option>
                                   <option value="3" <?php $selected = ""; (($param['bath']==3)? $selected='selected' : $selected="") ?> {{$selected}}>3+</option>
                                 </select>
                               </div>
                               <div class="col-md-12 form-group">
                                 <div class="row">
                                   <div class="col-md-12 form-group">
                                     <select name="min_pricem" id="min_pricem" class="form-control w-100">
                                       <option value="">Min Price *</option>
                                       <option value="100000000" <?php $selected = ""; (($param['min_p']==100000000)? $selected='selected' : $selected="") ?> {{$selected}}>100.000.000</option>
                                       <option value="500000000" <?php $selected = ""; (($param['min_p']==500000000)? $selected='selected' : $selected="") ?> {{$selected}}>500.000.000</option>
                                       <option value="1000000000" <?php $selected = ""; (($param['min_p']==1000000000)? $selected='selected' : $selected="") ?> {{$selected}}>1.000.000.000</option>
                                       <option value="5000000000" <?php $selected = ""; (($param['min_p']==5000000000)? $selected='selected' : $selected="") ?> {{$selected}}>5.000.000.000</option>
                                       <option value="10000000000" <?php $selected = ""; (($param['min_p']==10000000000)? $selected='selected' : $selected="") ?> {{$selected}}>10.000.000.000</option>
                                     </select>
                                   </div>
                                   <div class="col-md-12">
                                     <select name="max_pricem" id="max_pricem" class="form-control w-100">
                                       <option value="">Max Price *</option>
                                       <option value="100000000" <?php $selected = ""; (($param['max_p']==100000000)? $selected='selected' : $selected="") ?> {{$selected}}>100.000.000</option>
                                       <option value="500000000" <?php $selected = ""; (($param['max_p']==500000000)? $selected='selected' : $selected="") ?> {{$selected}}>500.000.000</option>
                                       <option value="1000000000" <?php $selected = ""; (($param['max_p']==1000000000)? $selected='selected' : $selected="") ?> {{$selected}}>1.000.000.000</option>
                                       <option value="5000000000" <?php $selected = ""; (($param['max_p']==5000000000)? $selected='selected' : $selected="") ?> {{$selected}}>5.000.000.000</option>
                                       <option value="10000000000" <?php $selected = ""; (($param['max_p']==10000000000)? $selected='selected' : $selected="") ?> {{$selected}}>10.000.000.000</option>
                                     </select>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                           </div>
                        </div>
                    </form> 
            </div>
            <div class="modal-footer">
              <div class="col-md-12">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Tutup</button>
                <a href="#submit" id="btn-wishlist-modal" class="btn btn-primary pull-right" onclick=addtoWishlist() style="margin-right: 1em;">Add</a>     
              </div>
            </div>
          </div>
      </div>
    </div>
<div class="ftco-blocks-cover-1">
      <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-7">
              <h1 class="mb-2">Our Properties</h1>
              <p class="text-white"></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @include('pages.property.property_search')
    @if(Auth::check())
    <div class="site-section-sm bg-black">
      <div class="container">
        <div class="row" style="margin-right: 4em; margin-top: 1em; position: absolute;   right: 0px;">
        
          <a style="background-color: #c10016;" href="#submit" id="btn-export" class="btn btn-primary text-white px-4 py-3">Export PDF</a>
          
        </div>
      </div>
    </div>
    @endif
    <div class="site-section bg-black">
      <div class="container">
        <div class="row">

          @foreach ($property as $val)
            <div class="col-md-4 mb-5">
              <div class="media-38289">
                <a href="https://galaxyproperty.co.id/property/detail/{{$val->number}}" class="d-block" target="blank"><img src="@if($val->path_images!=null)https://galaxyproperty.co.id/public/{{$val->path_images['small']}} @else {{asset('images')}}/default_image-1.jpg @endif" alt="Image" class="img-fluid"></a>
                <div class="text">
                  <div class="d-flex justify-content-between mb-3">
                    <div class="sq d-flex align-items-center"><span class="wrap-icon icon-fullscreen"></span> <span>{{$val->title}}</span></div>
                    <div class="bed d-flex align-items-center"><span class="wrap-icon icon-bed"></span> <span>{{$val->bedroom}}</span></div>
                    <div class="bath d-flex align-items-center"><span class="wrap-icon icon-bath"></span> <span>{{$val->bathroom}}</span></div>
                  </div>
                  <h3 class="mb-3"><a href="https://galaxyproperty.co.id/property/detail/{{$val->number}}" target="blank" style="color: #c10016">Rp. {{substr( number_format($val->price,2,',','.'),0,-3)}}</a></h3>
                  <span class="d-block small address d-flex align-items-center"> <span class="icon-room mr-3 text-primary"></span> <span>{{$val->address}}</span></span>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="row align-items-center justify-content-center  text-center pagination-39291" style="margin: auto; position: relative; text-align: center">
            <div class="pagination-39291">
              {{ $property->withPath('property')->links('vendor.pagination.bootstrap-4')}}
            </div>
              
        </div>
        @if(Auth::check())
        @if($is_search)
        <div class="row bg-black" style="margin-right: 4em; margin-top: 1em; position: absolute;   right: 0px; background-color: #c10016;">
          <div class="container">
              <a href="#submit" id="btn-add" style="background-color: #c10016;" class="btn btn-primary text-white px-4 py-3">Add to Wishlist</a>
          </div>
        </div>
         @endif  
        @endif
      </div>
    </div>
@endsection
@section('js_extras')
<script type="text/javascript">
$(document).ready(function() {
  var province_id = $("#province").val();
  getCity(province_id);
  switch($('#type').val()) {
    case "all":
      $("#all-tab").attr("class", "active");
      break;
    case "sell":
       $("#sale-tab").attr("class", "active");
      break;
    case "rent":
      $("#rent-tab").attr("class", "active");
      break;
  }

  $("#all-tab").click(function() {
        $('#type').val('all');
    });
  $("#rent-tab").click(function() {
        $('#type').val('rent');
    });
  $("#sale-tab").click(function() {
        $('#type').val('sell');
    });
  $("#province").change(function() {
    var province_id = $("#province").val();
    getCity(province_id);
  });
  $("#btn-export").on("click", function(e){
      e.preventDefault();
      $('#search-form').attr('action', "{{route('property/getPropertyPDF')}}").submit();
  });
  $("#btnAddWishlist").on("click", function(e){
      $('#modalWishlist').modal('show');
      $('#regionm').val($('#region').val());
      $('#titlem').val($('#title').val());
      $('#keywordm').val($('#keyword').val());
      switch($('#type').val()) {
        case "all":
          $("#all-tabm").attr("class", "active");
          break;
        case "sell":
           $("#sale-tabm").attr("class", "active");
          break;
        case "rent":
          $("#rent-tabm").attr("class", "active");
          break;
      }
      reloadProvinceAndCity($("#province").val(),$("#city").val())
      reloadCategoryType($("#propertyCategory").val());
  });
  $("#btn-add").on("click", function(e){
      e.preventDefault();
      $('#search-form').attr('action', "{{route('wishlist/addToWishlist')}}").submit();
      alert("pencarian berhasil ditambahkan ke Wishlist!");
  });

  $("#all-tabm").click(function() {
        $('#typem').val('all');
    });
  $("#rent-tabm").click(function() {
        $('#typem').val('rent');
    });
  $("#sale-tabm").click(function() {
        $('#typem').val('sell');
    });
  $("#provincem").change(function() {
    var province_idm = $("#provincem").val();
    getCityModal(province_idm);
  });
  $("#btn-wishlist-modal").on("click", function(e){
      e.preventDefault();
      var is_required = true;
      if ($('#min_pricem').val() =="") {
        is_required = false;
        alert('Minimal Price harus diisi!');
      }
      if ($('#max_pricem').val()  =="") {
        is_required = false;
        alert('Maximal Price harus diisi!');
      }
      if ($('#keywordm').val()=="") {
        is_required = false;
        alert('Keyword Price harus diisi!');
      }
      if ($('#propertyCategorym').val()=="") {
        is_required = false;
        alert('Property Type harus diisi!');
      }
      if (is_required) {
        $('#search-form-modal').attr('action', "{{route('wishlist/addToWishlistModal')}}").submit();
        $('#modalWishlist').modal('hide');
        alert("pencarian berhasil ditambahkan ke Wishlist!");
      }   
      // $('#search-form-modal').attr('action', "{{route('wishlist/addToWishlistModal')}}").submit();
      //   $('#modalWishlist').modal('hide');
      //   alert("pencarian berhasil ditambahkan ke Wishlist!");  
  });
});
function reloadProvinceAndCity(province_idp,city_idp){
          $.ajax({
              url: "{{route('property/getProvince')}}",
              data: {
                "_token": "{{ csrf_token() }}"
              },
              success: function(result) {
                console.log(result);
                var data = JSON.parse(result);
                $('#provincem').empty();
                $('#provincem').append('<option value="">Province</option>');
                var province_id = province_idp;
                for (var i = 0; i <= data.length; i++) {
                    if (province_id) {
                        if (province_id == data[i].id) {
                            $('#provincem').append('<option value="' + data[i].id + '" selected="selected">' + data[i].name + '</option>');
                        }
                        else{
                            $('#provincem').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                        }
                    }
                    else{
                        $('#provincem').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                    }
                    
                  
                }
              }
          });

          $.ajax({
              url: "{{route('property/getCity')}}",
              data: {
                'province_id' : province_idp,
                "_token": "{{ csrf_token() }}"
              },
              success: function(result) {
                console.log(result);
                var data = JSON.parse(result);
                $('#citym').empty();
                $('#citym').append('<option value="">City</option>');
                var city_id = city_idp;
                for (var i = 0; i <= data.length; i++) {
                    if (city_id) {
                        if (city_id == data[i].id) {
                            $('#citym').append('<option value="' + data[i].id + '" selected="selected">' + data[i].name + '</option>');
                        }
                        else{
                            $('#citym').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                        }
                    }
                    else{
                        $('#citym').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                    }
                    
                  
                }
              }
          });
}
function reloadCategoryType(id){


      //var data = JSON.parse(result);
      var data = <?php echo json_encode($propertyCategory); ?>;
      $('#propertyCategorym').empty();
      $('#propertyCategorym').append('<option value="">Type</option>');
      var type_id = id;
      for (var i = 0; i <= data.length; i++) {
        if (type_id) {
          if (type_id == data[i].id) {
            $('#propertyCategorym').append('<option value="' + data[i].id + '" selected="selected">' + data[i].name + '</option>');
          }
          else{
            $('#propertyCategorym').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
          }
        }
        else{
          $('#propertyCategorym').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
        }  
      }
}
</script>
@endsection
