<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="width=device-width" name="viewport"/>
<!--[if !mso]><!-->
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<!--<![endif]-->
<title></title>
<!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Bitter" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!--<![endif]-->
<style type="text/css">
    body {
      margin: 0;
      padding: 0;
    }

    table,
    td,
    tr {
      vertical-align: top;
    }

    * {
      line-height: inherit;
    }

    a[x-apple-data-detectors=true] {
      color: inherit !important;
      text-decoration: none !important;
    }
  </style>
<style id="media-query" type="text/css">
    @media only screen and (max-width: 660px) {
        .mobile-listing {
            display: block;
        }
        .pc-listing{
        display: none;
      }
    }
    @media only screen and (min-width: 661px) {
        .mobile-listing {
            display: none;
        }
        .pc-listing{
        display: block;
      }
    }
    @media (max-width: 660px) {
      .block-grid,
      .col {
        min-width: 320px !important;
        max-width: 100% !important;
        display: block !important;
      }

      .block-grid {
        width: 100% !important;
      }

      .col {
        width: 100% !important;
      }

      .col>div {
        margin: 0 auto;
      }

      img.fullwidth,
      img.fullwidthOnMobile {
        max-width: 100% !important;
      }

      .no-stack .col {
        min-width: 0 !important;
        display: table-cell !important;
      }

      .no-stack.two-up .col {
        width: 50% !important;
      }

      .no-stack .col.num4 {
        width: 33% !important;
      }

      .no-stack .col.num8 {
        width: 66% !important;
      }

      .no-stack .col.num4 {
        width: 33% !important;
      }

      .no-stack .col.num3 {
        width: 25% !important;
      }

      .no-stack .col.num6 {
        width: 50% !important;
      }

      .no-stack .col.num9 {
        width: 75% !important;
      }

      .video-block {
        max-width: none !important;
      }

      .mobile_hide {
        min-height: 0px;
        max-height: 0px;
        max-width: 0px;
        display: none;
        overflow: hidden;
        font-size: 0px;
      }

      .desktop_hide {
        display: block !important;
        max-height: none !important;
      }
    }
  </style>
</head>
<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #dde4ea;">
<!--[if IE]><div class="ie-browser"><![endif]-->
<table bgcolor="#dde4ea" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #dde4ea; width: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td style="word-break: break-word; vertical-align: top;" valign="top">

<div style="background-color:transparent;">
<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;">
<div style="display: table;width: 100%;background-color:#ffffff;">

<div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<div></div>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<div style="background-color:transparent;">
<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #7eaccd;">
<div style="display: table;width: 100%;background-color:#7eaccd;">

<div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<div></div>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<div style="background-color:transparent;">
<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;">
<div style="display: table;width: 100%;background-color:#ffffff;">

<div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px; background-image: url('http://agent.propertilaris.com/public/images/title-hitam-putih.jpg');">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;" valign="top">
<table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="50" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 50px; width: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td height="50" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman', serif;line-height:1.2;padding-top:10px;padding-right:20px;padding-bottom:10px;padding-left:20px;">
<div align="center" style="line-height: 1.2; font-size: 12px; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; color: #393d47; mso-line-height-alt: 14px;">
<img class="row align-items-center justify-content-center  text-center" align="center" width="70" height="120" src="http://agent.propertilaris.com/public/images/Logo-G-800.png">
</div>
</div>

<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman', serif;line-height:1.2;padding-top:10px;padding-right:20px;padding-bottom:10px;padding-left:20px;">
<div style="line-height: 1.2; font-size: 12px; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; color: #393d47; mso-line-height-alt: 14px;">
<p style="font-size: 46px; line-height: 1.2; word-break: break-word; text-align: center; font-family: Bitter, Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 55px; margin: 0;"><span style="font-size: 36px; color: #c10016;">"Hallo"</span></p>
</div>
</div>

<div style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 22px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 26px; margin: 0;"><span style="font-size: 15px;">Nampaknya Property yang anda cari baru-baru ini sudah tersedia di daftar listing</span><br><span style="font-size: 15px;"></span></p>
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->
<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;" valign="top">
<table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="50" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0;  mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 0px solid transparent; height: 50px; width: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td height="50" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<div style="background-color:transparent;">
<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #ffffff;">
<div style="display: table;width: 100%;background-color:#ffffff;">

</div>
</div>
</div>
<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent; height: 30px;">
<div style="display: table;width: 100%;background-color:transparent;">
</div>
</div>
<div class="pc-listing">
<!--CARD PROPERTY-->
@foreach ($property as $key => $val)
@if($key%2==0)
<!--CARD 1-->
<div style="background-color:transparent;">
<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
<div style="display: table;width: 100%;background-color:transparent;">

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; background-color: white; width: 320px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px; margin-bottom: 2px;">

<!--type 1-->
<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman',serif;line-height:1.2;padding-top:15px;padding-right:0px;padding-bottom:5px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left;font-family: Bitter, Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 35px; margin: 0;"><span style="font-size: 15px; color: black;">For {{$val->transaction_type}}</span></p>
</div>
</div>
<!--title 1-->
<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman',serif;line-height:1.2;padding-top:0px;padding-right:0px;padding-bottom:10px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left;font-family: Bitter, Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 35px; margin: 0;"><span style="font-size: 18px; color: #c10016;"><b>{{$val->title}}</b></span></p>
</div>
</div>
<!--space 1-->
<div style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:15px;padding-right:30px;padding-bottom:5px;padding-left:30px; height: 10px;">
</div>
<!--Address 1-->
<div style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:15px;padding-right:30px;padding-bottom:5px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">{{$val->address}}</span></p>
</div>
</div>
<!--luas 1-->
<div class="container pull-left" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:30px; display: inline-block; margin-right: 60px;">
<div class="pull-left" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">LT: {{$val->land_area}}</span></p>
</div>
<div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">LB: {{$val->building_area}}</span></p>
</div>
</div>
<!--luas 1-->
<div class="container pull-right" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:30px; display: inline-block;">
  <div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
  <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black; text-align: right;">{{$val->bedroom}} Kamar Tidur</span></p>
  </div>
  <div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
  <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black; text-align: right;">{{$val->bathroom}} Kamar Mandi</span></p>
  </div>
</div>
  <hr style="margin-right: 30px; margin-left: 30px;margin-top: 0px;margin-bottom: 0px;">
<!--Harga 1-->
<div class="container" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px; margin-top: 0px; margin-bottom: 5px;">
<div align="left" class="pull-left" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px; display: inline-block; margin-right: 10px;">
<p style="font-size: 25px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><b><span style="font-size: 18px; color: black;">IDR {{substr( number_format($val->price,2,',','.'),0,-3)}}</b></span></p>
</div>
<div align="right" class="pull-right" style="padding-top:2px;padding-right:0px;padding-bottom:2px;padding-left:10px; display: inline-block;">
<!--Btn see detail 1-->
<a class="pull-right" href="https://galaxyproperty.co.id/property/detail/{{$val->number}}" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #000000; background-color: #c10016; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; width: auto; width: auto; border-top: 0px solid #c10016; border-right: 0px solid #c10016; border-bottom: 0px solid #c10016; border-left: 0px solid #c10016; padding-top: 0px; padding-bottom: 0px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:5px;padding-right:5px;font-size:20px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 32px;"><span data-mce-style="font-size: 20px; line-height: 40px;" style="font-size: 14px; line-height: 20px; color:white;"><strong>See Detail</strong></span></span></span></a>
<!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->

<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; background-color: white; width: 320px; height: 230px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<!--image 1-->
<div align="center" class="img-container center" style="padding-right: 0px;padding-left: 0px;">
<img height="250" align="center" alt="Galaxy Property" border="0" class="center autowidth" src="@if($val->path_images!=null)https://galaxyproperty.co.id/public/{{$val->path_images['large']}} @else https://galaxyproperty.co.id/public/images/Setting/41/default_image-1.jpg @endif" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 320px; height: 250px; max-width: 320px; display: block; object-fit: fill; background-position: center center; background-repeat: no-repeat" title="Galaxy Property" width="320"/>
<!--[if mso]></td></tr></table><![endif]-->
</div>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>

<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent; height: 30px;">
<div style="display: table;width: 100%;background-color:transparent;">
</div>
</div>
@else
<!--CARD 2-->
<div style="background-color:transparent;">
<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
<div style="display: table;width: 100%;background-color:transparent;">

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; background-color: white; width: 320px;  height: 230px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<!--Image 2-->
<div align="center" class="img-container center" style="padding-right: 0px;padding-left: 0px;">

<img height="250" align="center" alt="Galaxy Property" border="0" class="center autowidth" src="@if($val->path_images!=null)https://galaxyproperty.co.id/public/{{$val->path_images['large']}} @else https://galaxyproperty.co.id/public/images/Setting/41/default_image-1.jpg @endif" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 320px; height: 250px; max-width: 320px; display: block; object-fit: fill; background-position: center center; background-repeat: no-repeat;" title="Galaxy Property" width="320"/>
<!--[if mso]></td></tr></table><![endif]-->
</div>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; background-color: white; width: 320px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--Type 2-->
<!--type 2-->
<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman',serif;line-height:1.2;padding-top:15px;padding-right:0px;padding-bottom:5px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left;font-family: Bitter, Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 35px; margin: 0;"><span style="font-size: 15px; color: black;">For {{$val->transaction_type}}</span></p>
</div>
</div>
<!--title 2-->
<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman',serif;line-height:1.2;padding-top:0px;padding-right:0px;padding-bottom:10px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left;font-family: Bitter, Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 35px; margin: 0;"><span style="font-size: 18px; color: #c10016;"><b>{{$val->title}}</b></span></p>
</div>
</div>
<!--space 2-->
<div style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:15px;padding-right:30px;padding-bottom:5px;padding-left:30px; height: 10px;">
</div>
<!--Address 2-->
<div style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:15px;padding-right:30px;padding-bottom:5px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">{{$val->address}}</span></p>
</div>
</div>
<!--luas 2-->
<div class="container pull-left" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:30px; display: inline-block; margin-right: 60px;">
<div class="pull-left" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">LT: {{$val->land_area}}</span></p>
</div>
<div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">LB: {{$val->building_area}}</span></p>
</div>
</div>
<!--luas 2-->
<div class="container pull-right" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:30px; display: inline-block;">
  <div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
  <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black; text-align: right;">{{$val->bedroom}} Kamar Tidur</span></p>
  </div>
  <div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
  <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black; text-align: right;">{{$val->bathroom}} Kamar Mandi</span></p>
  </div>
</div>
  <hr style="margin-right: 30px; margin-left: 30px;margin-top: 0px;margin-bottom: 0px;">
<!--Harga 2-->
<div class="container" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px; margin-top: 0px; margin-bottom: 5px;">
<div align="left" class="pull-left" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px; display: inline-block; margin-right: 10px;">
<p style="font-size: 25px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><b><span style="font-size: 18px; color: black;">IDR {{substr( number_format($val->price,2,',','.'),0,-3)}}</b></span></p>
</div>
<div align="right" class="pull-right" style="padding-top:2px;padding-right:0px;padding-bottom:2px;padding-left:10px; display: inline-block;">
<!--Btn see detail 2-->
<a class="pull-right" href="https://galaxyproperty.co.id/property/detail/{{$val->number}}" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #000000; background-color: #c10016; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; width: auto; width: auto; border-top: 0px solid #c10016; border-right: 0px solid #c10016; border-bottom: 0px solid #c10016; border-left: 0px solid #c10016; padding-top: 0px; padding-bottom: 0px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:5px;padding-right:5px;font-size:20px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 32px;"><span data-mce-style="font-size: 20px; line-height: 40px;" style="font-size: 14px; line-height: 20px; color:white;"><strong>See Detail</strong></span></span></span></a>
<!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
</div>
</div>

<!--[if mso]></td></tr></table><![endif]-->

<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>

<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent; height: 30px;">
<div style="display: table;width: 100%;background-color:transparent;">
</div>
</div>
@endif
@endforeach
<!--END CARD PROPERTY-->
</div>

<div class="mobile-listing">
<!--CARD PROPERTY-->
@foreach ($property as $key => $val)
<!--CARD 2-->
<div style="background-color:transparent;">
<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
<div style="display: table;width: 100%;background-color:transparent;">

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; background-color: white; width: 320px;  height: 230px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<!--Image 2-->
<div align="center" class="img-container center" style="padding-right: 0px;padding-left: 0px;">

<img height="250" align="center" alt="Galaxy Property" border="0" class="center autowidth" src="@if($val->path_images!=null)https://galaxyproperty.co.id/public/{{$val->path_images['large']}} @else https://galaxyproperty.co.id/public/images/Setting/41/default_image-1.jpg @endif" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 320px; height: 250px; max-width: 320px; display: block; object-fit: fill; background-position: center center; background-repeat: no-repeat;" title="Galaxy Property" width="320"/>
<!--[if mso]></td></tr></table><![endif]-->
</div>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; background-color: white; width: 320px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--Type 2-->
<!--type 2-->
<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman',serif;line-height:1.2;padding-top:15px;padding-right:0px;padding-bottom:5px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left;font-family: Bitter, Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 35px; margin: 0;"><span style="font-size: 15px; color: black;">For {{$val->transaction_type}}</span></p>
</div>
</div>
<!--title 2-->
<div style="color:#393d47;font-family:'Bitter', Georgia, Times, 'Times New Roman',serif;line-height:1.2;padding-top:0px;padding-right:0px;padding-bottom:10px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: 'Bitter', Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left;font-family: Bitter, Georgia, Times, 'Times New Roman', serif; mso-line-height-alt: 35px; margin: 0;"><span style="font-size: 18px; color: #c10016;"><b>{{$val->title}}</b></span></p>
</div>
</div>
<!--space 2-->
<div style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:15px;padding-right:30px;padding-bottom:5px;padding-left:30px; height: 10px;">
</div>
<!--Address 2-->
<div style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:15px;padding-right:30px;padding-bottom:5px;padding-left:30px;">
<div style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">{{$val->address}}</span></p>
</div>
</div>
<!--luas 2-->
<div class="container pull-left" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:30px; display: inline-block; margin-right: 60px;">
<div class="pull-left" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">LT: {{$val->land_area}}</span></p>
</div>
<div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black;">LB: {{$val->building_area}}</span></p>
</div>
</div>
<!--luas 2-->
<div class="container pull-right" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:10px;padding-bottom:0px;padding-left:30px; display: inline-block;">
  <div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
  <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black; text-align: right;">{{$val->bedroom}} Kamar Tidur</span></p>
  </div>
  <div class="pull-right" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
  <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: right; mso-line-height-alt: 24px; margin: 0;"><span style="font-size: 15px; color: black; text-align: right;">{{$val->bathroom}} Kamar Mandi</span></p>
  </div>
</div>
  <hr style="margin-right: 30px; margin-left: 30px;margin-top: 0px;margin-bottom: 0px;">
<!--Harga 2-->
<div class="container" style="color:#393d47;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:0px;padding-right:30px;padding-bottom:0px;padding-left:30px; margin-top: 0px; margin-bottom: 5px;">
<div align="left" class="pull-left" style="line-height: 1.2; font-size: 12px; color: #393d47; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px; display: inline-block; margin-right: 10px;">
<p style="font-size: 25px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;"><b><span style="font-size: 18px; color: black;">IDR {{substr( number_format($val->price,2,',','.'),0,-3)}}</b></span></p>
</div>
<div align="right" class="pull-right" style="padding-top:2px;padding-right:0px;padding-bottom:2px;padding-left:10px; display: inline-block;">
<!--Btn see detail 2-->
<a class="pull-right" href="https://galaxyproperty.co.id/property/detail/{{$val->number}}" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #000000; background-color: #c10016; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; width: auto; width: auto; border-top: 0px solid #c10016; border-right: 0px solid #c10016; border-bottom: 0px solid #c10016; border-left: 0px solid #c10016; padding-top: 0px; padding-bottom: 0px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:5px;padding-right:5px;font-size:20px;display:inline-block;"><span style="font-size: 16px; line-height: 2; word-break: break-word; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 32px;"><span data-mce-style="font-size: 20px; line-height: 40px;" style="font-size: 14px; line-height: 20px; color:white;"><strong>See Detail</strong></span></span></span></a>
<!--[if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif]-->
</div>
</div>

<!--[if mso]></td></tr></table><![endif]-->

<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>

<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent; height: 30px;">
<div style="display: table;width: 100%;background-color:transparent;">
</div>
</div>
@endforeach
</div>

<div style="background-color:transparent;">
<div class="block-grid two-up" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
<div style="display: table;width: 100%;background-color:transparent;">

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; width: 320px; background-color:white;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;" valign="top">
<table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="30" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0;  mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 1px solid transparent; height: 30px; width: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td height="30" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;">
<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><![endif]--><img align="center" alt="Galaxy Property" border="0" class="center autowidth" src="http://agent.propertilaris.com/public/images/galaxy_logo_red.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 274px; display: block;" title="Galaxy Property" width="274"/>
<!--[if mso]></td></tr></table><![endif]-->
</div>

<div style="color:white;font-family:'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
<div style="line-height: 1.2; font-size: 12px; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; color: white; mso-line-height-alt: 14px;">
<p style="font-size: 14px; line-height: 1.2; text-align: center; word-break: break-word; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; mso-line-height-alt: 17px; margin: 0;"><span style="font-size: 14px; color: black">Your One Stop Property Solution</span></p>
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>

<div class="col num6" style="min-width: 320px; max-width: 320px; display: table-cell; vertical-align: top; width: 320px; background-color: white;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
<!--<![endif]-->
<table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0;  mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px;" valign="top">
<table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="30" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0;  mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-top: 1px solid transparent; height: 30px; width: 100%;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td height="30" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

<div style="color:white;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:1.2;padding-top:5px;padding-right:0px;padding-bottom:5px;padding-left:0px;">
<div style="line-height: 1.2; font-size: 12px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; color: white; mso-line-height-alt: 14px;">
<p style="font-size: 24px; line-height: 1.2; text-align: center; word-break: break-word; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; mso-line-height-alt: 29px; margin: 0;"><span style="font-size: 24px; color: #c10016"><strong><span style=""><span style="color: #c10016;">Connect With Us</span></span></strong></span></p>
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->
<table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
<tbody>
<tr style="vertical-align: top;" valign="top">
<td style="word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
<table align="center" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0;  mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" valign="top">
<tbody>
<tr align="center" style="vertical-align: top; display: inline-block; text-align: center;" valign="top">
<td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 2.5px; padding-left: 2.5px;" valign="top"><a href="https://www.facebook.com/id.galaxyproperty/" target="_blank"><img alt="Facebook" height="32" src="http://agent.propertilaris.com/public/images/facebook2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="facebook" width="32"/></a></td>
<td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 2.5px; padding-left: 2.5px;" valign="top"><a href="https://twitter.com/galaxy_property" target="_blank"><img alt="Twitter" height="32" src="http://agent.propertilaris.com/public/images/twitter2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="twitter" width="32"/></a></td>
<td style="word-break: break-word; vertical-align: top; padding-bottom: 0; padding-right: 2.5px; padding-left: 2.5px;" valign="top"><a href="https://www.instagram.com/galaxy.property/" target="_blank"><img alt="Instagram" height="32" src="http://agent.propertilaris.com/public/images/instagram2x.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="instagram" width="32"/></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!--[if mso]></td></tr></table><![endif]-->
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<div style="background-color:transparent;">
<div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
<div style="display: table;width: 100%;background-color:white;">

<div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
<div style="width:100% !important;">
<!--[if (!mso)&(!IE)]><!-->
<div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">

<div style="color:white;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:1.2;padding-top:20px;padding-right:20px;padding-bottom:20px;padding-left:20px;">
<div style="color: white; font-size: 12px; line-height: 1.2; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; mso-line-height-alt: 14px;">
<p style="font-size: 12px; line-height: 1.2; text-align: center; word-break: break-word; mso-line-height-alt: 14px; margin: 0;"><span style="color: black;">Changed your mind? You can <span style="font-size: 12px;"><a href="http://agent.propertilaris.com/wishlist" rel="noopener" style="text-decoration: underline; color: #c10016;" target="_blank">unsubscribe</a></span style="color: black;"> at any time.</span></p>
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
</td>
</tr>
</tbody>
</table>
<!--[if (IE)]></div><![endif]-->
</body>
</html>
