@extends('layouts.app')
@section('content')
<div class="ftco-blocks-cover-1">
      <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/Foto-Galaxy-desktop.jpg')}}')">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
  
          </div>
        </div>
      </div>
    </div>

    @include('pages.property.property_search')

    <div class="site-section">
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-lg-6">
            <div class="h-100 p-5 bg-black">
              <div class="row">
                <div class="col-md-6 text-center mb-4">
                  <div class="service-38201">
                    <span class="flaticon-house-2"></span>
                    <h3>Estate Insurance</h3>
                    <p>Estate Management</p>
                  </div>
                </div>
                <div class="col-md-6 text-center mb-4">
                  <div class="service-38201">
                    <span class="flaticon-bathtub"></span>
                    <h3>Elegant Bathtub</h3>
                    <p>Estate Management</p>
                  </div>
                </div>
                <div class="col-md-6 text-center mb-4">
                  <div class="service-38201">
                    <span class="flaticon-house-1"></span>
                    <h3>Fresh Air</h3>
                    <p>Estate Management</p>
                  </div>
                </div>
                <div class="col-md-6 text-center mb-4">
                  <div class="service-38201">
                    <span class="flaticon-calculator"></span>
                    <h3>Estate Calculator</h3>
                    <p>Estate Management</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5 ml-auto">
            <h3 class="heading-29201">About Us</h3>
            
            <p class="mb-5">Perspiciatis quidem harum provident repellat sint.</p>

            <div class="service-39290 d-flex align-items-start mb-5">
              <div class="media-icon mr-4">
                <span class="flaticon-house-1"></span>
              </div>
              <div class="text">
                <h3>Vission</h3>
                <ul>
                  <li><p>Menjadi agen properti yang terbaik dan terpercaya sebagai market leader bereputasi baik dan berprestasi.</p></li>
                  <li><p>Menjadi agen properti pilihan utama konsumen.</p></li>
                  <li><p>Melayani kebutuhan masyarakat sebagai konsultan di bidang informasi properti dan memastikan standar layanan setinggi-tingginya.</p></li>
                  <li><p>Meningkatkan kebersamaan, kekeluargaan dan kesejahteraan seluruh anggota GALAXY.</p></li>
                </ul>
              </div>
            </div>

            <div class="service-39290 d-flex align-items-start mb-5">
              <div class="media-icon  mr-4">
                <span class="flaticon-calculator"></span>
              </div>
              <div class="text">
                <h3>Mission</h3>
                <ul>
                  <li><p>Kreatif mengembangkan dan menerapkan inovasi baru dan solusi yang efektif dalam produk dan layanan di bidang properti.</p></li>
                  <li><p>1st in Service, 1st in Results dan 1st in Customer Satisfaction.</p></li>
                  <li><p>Menghasilkan marketing associates yang handal, berpengalaman, berpikiran jauh ke depan, berorientasi pada hasil, melakukan pemasaran dengan pendekatan proaktif, mengenal kejujuran yang tinggi, etika bisnis sehingga klien sepenuhnya percaya bahwa mereka dibantu oleh agen profesional yang memberikan konsultasi dengan layanan dan integritas yang terbaik.</p></li>
                  <li><p>Meningkatkan skill dan integritas para marketing associates dengan pelatihan dan perangkat pemasaran terbaik sehingga sanggup membantu proses transaksi properti secara efisien dan transparan serta berorientasi pada konsep win-win solution.</p></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection
    @section('js_extras')
<script type="text/javascript">
$(document).ready(function() {
  switch($('#type').val()) {
    case "all":
      $("#all-tab").attr("class", "active");
      break;
    case "sell":
       $("#sale-tab").attr("class", "active");
      break;
    case "rent":
      $("#rent-tab").attr("class", "active");
      break;
  }
  $("#all-tab").click(function() {
        $('#type').val('all');
    });
  $("#rent-tab").click(function() {
        $('#type').val('rent');
    });
  $("#sale-tab").click(function() {
        $('#type').val('sell');
    });
  $("#province").change(function() {
      var province_id = $("#province").val();
      getCity(province_id);
  });
  
});
</script>
@endsection

    
