<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <link rel="icon" href="http://agent.propertilaris.com/public/images/Logo-G-800.png">
        <title>Galaxy Property</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <link href="https://fonts.googleapis.com/css?family=DM+Sans:300,400,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/fonts/icomoon/style.css">

        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/css/jquery.fancybox.min.css">
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/fonts/flaticon/font/flaticon.css">
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/css/aos.css">

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
       
        <script src="{{ asset('assets/canvasjs')}}/jquery-1.11.1.min.js"></script>
        <script src="{{ asset('assets/canvasjs')}}/jquery.canvasjs.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        
        <!-- MAIN CSS -->
        <link rel="stylesheet" href="{{ asset('assets/realtors')}}/css/style.css">
        @yield('style')
    </head>
    <body  data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
        <div class="site-wrap" id="home-section">

              <div class="site-mobile-menu site-navbar-target">
                <div class="site-mobile-menu-header">
                  <div class="site-mobile-menu-close mt-3">
                    <span class="icon-close2 js-menu-toggle"></span>
                  </div>
                </div>
                <div class="site-mobile-menu-body"></div>
              </div>

            <!-- Page Heading -->
            @include('pages.includes.header')

            <!-- Page Content -->
            @yield('content')
        @include('pages.includes.footer')
        <script src="{{ asset('assets/realtors')}}/js/jquery-3.3.1.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/jquery-migrate-3.0.0.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/popper.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/bootstrap.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/owl.carousel.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/jquery.sticky.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/jquery.waypoints.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/jquery.animateNumber.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/jquery.fancybox.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/jquery.stellar.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/jquery.easing.1.3.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/bootstrap-datepicker.min.js"></script>
        <script src="{{ asset('assets/realtors')}}/js/aos.js"></script>

        <script src="{{ asset('assets/realtors')}}/js/main.js"></script>

        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>


    </div>
    <script type="text/javascript">
        function getCity(province_id){
        $.ajax({
              url: "{{route('property/getCity')}}",
              data: {
                'province_id' : province_id,
                "_token": "{{ csrf_token() }}"
              },
              success: function(result) {
                console.log(result);
                var data = JSON.parse(result);
                $('#city').empty();
                $('#city').append('<option value="">City</option>');
                var city_id = <?PHP echo (!empty($param['city']) ? json_encode($param['city']) : '""'); ?>;
                for (var i = 0; i <= data.length; i++) {
                    if (city_id) {
                        if (city_id == data[i].id) {
                            $('#city').append('<option value="' + data[i].id + '" selected="selected">' + data[i].name + '</option>');
                        }
                        else{
                            $('#city').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                        }
                    }
                    else{
                        $('#city').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                    }
                    
                  
                }
              }
        });
      }
      function getCityModal(province_id){
        $.ajax({
              url: "{{route('property/getCity')}}",
              data: {
                'province_id' : province_id,
                "_token": "{{ csrf_token() }}"
              },
              success: function(result) {
                console.log(result);
                var data = JSON.parse(result);
                $('#citym').empty();
                $('#citym').append('<option value="">City</option>');
                var city_id = <?PHP echo (!empty($param['city']) ? json_encode($param['city']) : '""'); ?>;
                for (var i = 0; i <= data.length; i++) {
                    if (city_id) {
                        if (city_id == data[i].id) {
                            $('#citym').append('<option value="' + data[i].id + '" selected="selected">' + data[i].name + '</option>');
                        }
                        else{
                            $('#citym').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                        }
                    }
                    else{
                        $('#citym').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
                    }
                    
                  
                }
              }
        });
      }
    </script>
    @yield('js_extras')
    </body>
</html>