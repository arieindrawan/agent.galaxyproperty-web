<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Property;
use App\Models\User;
use App\Models\Wishlist;
use App\Models\Vidio;
use App\Models\PropertyCategory;
use App\Models\Province;
use App\Models\City;
use Illuminate\Support\Facades\Mail;
use App\Mail\WishlistEmail;
use DB;
use Illuminate\Support\Facades\Http;

class WishlistCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:wishlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cron untuk mengirim listing wishlist yang baru diinput';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set("Asia/Jakarta");

        $start_hour = date("Y-m-d H:i:s", strtotime ("-1 hour"));
        $end_hour =  date("Y-m-d H:i:s");

        $param_data = [];
        $param_data['start_hour'] = $start_hour;
        $param_data['end_hour'] = $end_hour;
        $param_data['order'] = 'N2O';
        $param_data['page'] = 1;
        $perPage = 0;
        $param_data['limit'] = $perPage;
        // $param_data['limit'] = 0;
        $dataApiProperty =  Http::get('https://galaxyproperty.co.id/property/getPropertyAPI',$param_data);
        $dataApiProperty = json_decode($dataApiProperty);
        $data_property = $dataApiProperty->property;


        if (!empty($data_property)) {
            $data_wishlist = Wishlist::get();

            foreach ($data_wishlist as $key1 => $value_wishlist) {

                $param = "Ini adalah Property dari wishlist anda";

                $param_api = [];
                if (!empty($value_wishlist->type)) {
                    if ($value_wishlist->type!="all"){
                        $param_api['type'] = $value_wishlist->type;
                    }
                    if ($value_wishlist->propertycategory_id != ""){
                        $param_api['propertyCategory'] = $value_wishlist->propertycategory_id; 
                    }
                    if ($value_wishlist->title != ""){
                        $param_api['title'] = $value_wishlist->title;
                    }
                    if ($value_wishlist->keyword != ""){
                        $param_api['keyword'] = $value_wishlist->keyword;
                    }
                    if ($value_wishlist->province_id != ""){
                        $param_api['provinceId'] = $value_wishlist->province_id;
                    }
                    if ($value_wishlist->city_id != ""){
                        $param_api['cityId'] = $value_wishlist->city_id;
                    }
                    if ($value_wishlist->region != ""){
                        $param_api['region'] = $value_wishlist->region;
                    }
                    if ($value_wishlist->bedroom != ""){
                        if ($value_wishlist->bedroom == "3") {
                            $param_api['bed'] = 4;
                        }
                        else{
                            $param_api['bed'] = $value_wishlist->bedroom;
                        }
                    }
                    if ($value_wishlist->bathroom != ""){
                        if ($value_wishlist->bathroom == "3") {
                            $param_api['bath'] = 4;
                        }
                        else{
                            $param_api['bath'] = $value_wishlist->bathroom;
                        }
                    }
                    if ($value_wishlist->min_price != ""){
                        $param_api['price_min'] = $value_wishlist->min_price;
                    }
                    if ($value_wishlist->max_price != ""){
                        if ($value_wishlist->max_price!="~") {
                            $param_api['price_max'] = $value_wishlist->max_price;
                        }
                        
                    }
                }


                // set limit 
                $perPage = 0;

                $param_api['start_hour'] = $start_hour;
                $param_api['end_hour'] = $end_hour;
                $param_api['order'] = 'N2O';
                $param_api['limit'] = $perPage;
                //dump($param_api);
                $dataApi =  Http::get('https://galaxyproperty.co.id/property/getPropertyAPI',$param_api);
                $dataApi = json_decode($dataApi);

                $property = $dataApi->property;

                if (!empty($value_wishlist->type)) {
                        if ($value_wishlist->type!="all"){
                            $param.= "dengan transaction type ".$value_wishlist->type.",";
                        }
                        if ($value_wishlist->propertycategory_id != ""){
                            $p = PropertyCategory::find($value_wishlist->propertycategory_id);
                            $param.= " dengan kategori ".$p->name.",";
                        }
                        if ($value_wishlist->title != ""){
                            $param.= " title : ".$value_wishlist->title.",";
                        }
                        if ($value_wishlist->keyword != ""){
                            $param.= " keyword : ".$value_wishlist->keyword.",";
                        }
                        if ($value_wishlist->province_id != ""){
                            $p = Province::find($value_wishlist->province_id);
                            $param.= " di provinsi : ".$p->name.",";
                        }
                        if ($value_wishlist->city_id != ""){
                            $p = City::find($value_wishlist->city_id);
                            $param.= " Kota : ".$p->name.",";
                        }
                        if ($value_wishlist->region != ""){
                            $param.= " region : ".$value_wishlist->region.",";
                        }
                        if ($value_wishlist->bedroom != ""){
                            if ($value_wishlist->bedroom == "3") {
                                $param.= " jumlah bedroom lebih dari : ".$value_wishlist->bedroom.",";
                            }
                            else{
                                $param.= " jumlah bedroom : ".$value_wishlist->bedroom.",";
                            }
                        }
                        if ($value_wishlist->bathroom != ""){
                            if ($value_wishlist->bathroom == "3") {
                                 $param.= " jumlah bathroom lebih dari: ".$value_wishlist->bathroom.",";
                            }
                            else{
                                 $param.= " jumlah bathroom : ".$value_wishlist->bathroom.",";
                            }
                        }
                        if ($value_wishlist->min_price != ""){
                            $param.= " min price: ".$value_wishlist->min_price.",";
                        }
                        if ($value_wishlist->max_price != ""){
                            if ($value_wishlist->max_price!="~") {
                               $param.= " max price: ".$value_wishlist->max_price.",";
                            }
                                
                        }
                    }

                    foreach ($property as $key => $value) {
                        $str_img = json_decode($value->images,true);
                        $value->path_images = $str_img; 
                    }
                    if(substr($param,-1) ==","){
                        $param = substr_replace($param,".",-1);
                    }
                if (count($property) > 0) {
                    foreach ($property as $key => $value) {
                        $words = explode(" ",$value->title);
                        $value->title = implode(" ",array_splice($words,0,5));
                    }
                    $data['property'] = $property;
                    $data['data_wishlist'] = $value_wishlist;
                    $data['param'] = $param;
                    $data['user'] = User::find($value_wishlist->user_id);
                    Mail::to($data['user']->email)->send(new WishlistEmail($data));
                }
            }
        }
    }
}
