<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\BirthdayEmail;

class BirthdayCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cron untuk mengirim birthday sertificate ke pc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set("Asia/Jakarta");
        $date = now();
        $data_user= User::whereMonth('birthdate', '=', $date->month)->whereDay('birthdate', '=', $date->day)->get();

        if (count($data_user)>0) {
            foreach ($data_user as $key => $value) {
                $data['user'] = $value;
                Mail::to($data['user']->email)->send(new BirthdayEmail($data));
            }
        }
    }
}
