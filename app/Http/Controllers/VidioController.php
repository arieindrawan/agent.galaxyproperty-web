<?php

namespace App\Http\Controllers;

use App\Models\Vidio;
use Illuminate\Http\Request;
use App\Models\ReportGst;

class VidioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vidios = Vidio::latest()->paginate(5);

        return view('pages.vidios.index', compact('vidios'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.vidios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $link = $request->input('link_url');
        // substr($link,strpos($mystring, '/'),);
        $stat = false;
        $code = "";
        for ($i=0; $i < strlen($link) ; $i++) { 
            if ($link[$i]=='=') {
                $stat = true;
                continue;
            }
            if ($link[$i]=='&') {
                $stat = false;
                break;
            }
            if ($stat) {
                $code.=$link[$i];
            }
        }
        $request->validate([
            'name' => 'required',
            'duration' => 'required',
            'link_url' => 'required'
        ]);
        $vidio['name'] = $request->input('name');
        $vidio['duration'] = $request->input('duration');
        $vidio['link_url'] = $link;
        $vidio['code'] = $code;
        Vidio::create($vidio);

        return redirect()->route('vidios.index')
            ->with('success', 'Vidio created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vidio  $vidio
     * @return \Illuminate\Http\Response
     */
    public function show(Vidio $vidio)
    {
        return view('pages.vidios.show', compact('vidio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vidio  $vidio
     * @return \Illuminate\Http\Response
     */
    public function edit(Vidio $vidio)
    {
        return view('pages.vidios.edit', compact('vidio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vidio  $vidio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vidio $vidio)
    {
        $link = $request->input('link_url');
        // substr($link,strpos($mystring, '/'),);
        $stat = false;
        $code = "";
        for ($i=0; $i < strlen($link) ; $i++) { 
            if ($link[$i]=='=') {
                $stat = true;
                continue;
            }
            if ($link[$i]=='&') {
                $stat = false;
                break;
            }
            if ($stat) {
                $code.=$link[$i];
            }
        }
        $request->validate([
            'name' => 'required',
            'duration' => 'required',
            'link_url' => 'required'
        ]);
        $vidion['name'] = $request->input('name');
        $vidion['duration'] = $request->input('duration');
        $vidion['link_url'] = $link;
        $vidion['code'] = $code;

        $vidio->update($vidion);

        return redirect()->route('vidios.index')
            ->with('success', 'Vidio updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vidio  $vidio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vidio $vidio)
    {
        $report = ReportGst::where('vidio_id',$vidio->id)->get();
        foreach ($report as $key => $value) {
            $value->delete();
        }
        $vidio->delete();
        return redirect()->route('vidios.index')
                        ->with('success','Vidio deleted successfully');
    }

}
