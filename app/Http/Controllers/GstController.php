<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vidio;
use App\Models\ReportGst;
use Auth;
use DB;
class GstController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $vidios = Vidio::all();
            return view('pages.gst.index',compact('vidios'));
        }
        else{
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function report()
    {
        $vidios = Vidio::latest()->paginate(5);

        return view('pages.vidios.index', compact('vidios'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function singleVidio($id)
    {
        date_default_timezone_set('Asia/Jakarta');
        if ($id) {
            $report = new ReportGst;
            $report->user_id = Auth::user()->id;
            $report->vidio_id = $id;
            $report->opened_at = date("Y-m-d H:i:s");
            $report->is_done = 0;
            $report->save();

            $vidios = Vidio::find($id);

            $data['vidios'] = $vidios;
            $data['report'] = $report;
            return view('pages.gst.single_vidio',$data);
        }
    }
    public function updateReport(Request $request)
    {
        if ($request->report_id) {
            $report = ReportGst::find($request->report_id);
            $report->is_done = 1;
            $report->save();
        }
        return true;
    }
    public function getAgentReport(Request $request)
    {
        $start = null;
        $end = null;
        if ($request->start) {
            $start = $request->start;
        }
        if ($request->end) {
            $end = $request->end;
        }
        $reports = ReportGst::getAgentReport($start,$end);
        $str = "    <div class='row'>
                      <div class='col-md-3 form-group'>
                      <label class='control-label' for='start_date'>Start
                                  </label>
                       <input id='start_date' class='form-control' name='start_date' placeholder='e.g ' type='date' value='$start'>
                     </div>
                     <div class='col-md-3 form-group'>
                     <label class='control-label' for='end_date'>End
                                  </label>
                       <input id='end_date' class='form-control' name='end_date' placeholder='e.g ' type='date' value='$end'>
                     </div>
                     <div class='col-md-3 form-group'>
                        <button style='margin-top:2.5em;' onclick=getAgentReport() type='button' class='btn btn-primary text-white px-2 py-2'>Filter</button>
                     </div>
                   </div>
                    <table id='mytable' class='table table-striped table-hover' style='cursor: pointer;'>
                        <thead>
                          <tr>
                            <th>Nama</th>
                            <th>Vidio Ditonton</th>
                          </tr> 
                        </thead>
                        <tbody>";
                        $str_val = "";
                        foreach ($reports as $report){
                            $str_val .= 
                          "<tr onclick=detailAgent('".route('gst/detail_agent', ['id'=>$report->user_id])."') class='clickable-row' data-href=".route('gst/detail_agent', ['id'=>$report->user_id]).">
                              <td width='60%' class='align-middle'>
                                  ".$report->agent_name."
                              </td>
                              <td width='40%' class='align-middle'>
                                 ".$report->vidio_ditonton."
                              </td>
                          </tr>";
                        }
        $result= $str.$str_val."</tbody> </table>";
        return $result;
    }
    public function getVidioReport(Request $request)
    {
        $reports = ReportGst::getVidioReport();
        $str = "<table id='mytable' class='table table-striped table-hover' style='cursor: pointer;'>
                        <thead>
                          <tr>
                              <th>Title</th>
                              <th>Jumlah Penonton</th>
                          </tr> 
                        </thead>
                        <tbody>";
                        $str_val = "";
                        foreach ($reports as $report){
                            $str_val .= 
                          "<tr onclick=detailVidio('".route('gst/detail_vidio', ['id'=>$report->vidio_id])."') class='clickable-row' data-href=".route('gst/detail_vidio', ['id'=>$report->vidio_id]).">
                              <td width='60%' class='align-middle'>
                                  ".$report->vidio_name."
                              </td>
                              <td width='40%' class='align-middle'>
                                 ".$report->user_menonton."
                              </td>
                          </tr>";
                        }
        $result= $str.$str_val."</tbody> </table>";
        return $result;
    }
    public function getStatistic(Request $request)
    {
       DB::enableQueryLog();
        $start = null;
        $end = null;
        if ($request->start) {
            $start = $request->start;
        }
        if ($request->end) {
            $end = $request->end;
        }

        $_1 = ReportGst::selectRaw("'00:00-03:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [0, 2])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });
        $_2 = ReportGst::selectRaw("'03:00-06:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [3, 5])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });
        $_3 = ReportGst::selectRaw("'06:00-09:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [6, 8])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });
        $_4 = ReportGst::selectRaw("'09:00-12:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [9, 11])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });
        $_5 = ReportGst::selectRaw("'12:00-15:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [12, 14])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });
        $_6 = ReportGst::selectRaw("'15:00-18:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [15, 17])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });
        $_7 = ReportGst::selectRaw("'18:00-21:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [18, 20])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });
        $_8 = ReportGst::selectRaw("'21:00-24:00' as jam, COUNT(id) as jumlah_penonton")->whereBetween(DB::raw("Hour(opened_at)"), [21, 23])->where(function($query) use ($start, $end)
            {
                if ($start!=null){
                    $query->where('opened_at','>=',$start."");
                }
                if ($end!=null){
                    $query->where('opened_at','<=',$end."");
                }
            });

        $results = $_1->union($_2)->union($_3)->union($_4)->union($_5)->union($_6)->union($_7)->union($_8)->get();
         
        $arrResult = [];
        foreach ($results as $key => $value) {
            $arrResult[] = $value->jumlah_penonton;
        }
        return $arrResult;
        //return DB::getQueryLog();
    }
    public function detailAgent($id)
    {
        $finalResult = array();
        if ($id) {
            $result = ReportGst::getDetailAgent($id);
            return $result;
        }
    }
    public function detailVidio($id)
    {
        $finalResult = array();
        if ($id) {
            $result = ReportGst::getDetailVidio($id);
            return $result;
        }
    }
}
