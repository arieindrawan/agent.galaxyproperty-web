<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Property;
use App\Models\Province;
use App\Models\City;
use App\Models\PropertyCategory;
use Illuminate\Pagination\Paginator;
use App\Mail\WishlistEmail;


class HomeController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$data['province'] = Province::all();
        $data['city'] = City::all();
        $data['propertyCategory'] = PropertyCategory::all();

        $is_search = false;

        $transaction_type = $request->input('type');
        $property_type = $request->input('propertyCategory');
        $title = $request->input('title');
        $keyword = $request->input('keyword');
        $province = $request->input('province');
        $city = $request->input('city');
        $region = $request->input('region');
        $bed = $request->input('bed');
        $bath = $request->input('bath');
        $min_p = $request->input('min_price');
        $max_p = $request->input('max_price');

        $param['transaction_type'] = $transaction_type;
        $param['property_type'] = $property_type;
        $param['title'] = $title;
        $param['keyword'] = $keyword;
        $param['province'] = $province;
        $param['city'] = $city;
        $param['region'] = $region;
        $param['bed'] = $bed;
        $param['bath'] = $bath;
        $param['min_p'] = $min_p;
        $param['max_p'] = $max_p;

        if ($transaction_type!="") {
            $is_search = true;
        }
        $data['is_search'] = $is_search;

        $data['param'] = $param;
        return view('pages.home.index', $data);

    }
    public function sendEmail(){

        // $data = array('nama' =>'ARIE' ,'website'=>'galaxyproperty.co.id');
        // // Mail::send('pages.mail.wishlist_email',$data, function($message) use ($data){
        // //     $message->to('arie.indrawan4@gmail.com',$data['nama'])->subject('testing mailgun');
        // // });
        Mail::to("arie.indrawan4@gmail.com")->send(new WishlistEmail());

        return "Email telah dikirim";
    }
}
