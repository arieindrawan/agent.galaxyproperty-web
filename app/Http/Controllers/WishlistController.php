<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Property;
use App\Models\Province;
use App\Models\City;
use App\Models\PropertyCategory;
use App\Models\Wishlist;
use Auth;

class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$where[] =['wishlist_requests.user_id','=',Auth::user()->id]; 
        $wishlist = Wishlist::getWihslist($where,null);
        $data['wishlists'] = $wishlist;
        return view('pages.wishlist.index', $data);
    }
    public function show(Wishlist $wishlist)
    {
        return view('pages.wishlist.show', compact('wishlist'));
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['province'] = Province::all();
        $data['city'] = City::all();
        $data['propertyCategory'] = PropertyCategory::all();
        return view('pages.wishlist.create',$data);
    }
    public function store(Request $request)
    {

        $transaction_type = $request->input('type');
        $property_type = $request->input('propertyCategory');
        $title = $request->input('title');
        $keyword = $request->input('keyword');
        $province = $request->input('province');
        $city = $request->input('city');
        $is_wishlist = $request->input('is_wishlist');
        $region = $request->input('region');
        $bed = $request->input('bed');
        $bath = $request->input('bath');
        $min_p = $request->input('min_price');
        $max_p = $request->input('max_price');

        if (Auth::check()) {
            $wishlist = new Wishlist();
            $wishlist->user_id = Auth::user()->id;
            $wishlist->title = (!empty($title)) ? $title:null;
            $wishlist->type = (!empty($transaction_type)) ? $transaction_type:null; 
            $wishlist->propertycategory_id = (!empty($property_type)) ? $property_type:null; 
            $wishlist->province_id = (!empty($province)) ? $province:null; 
            $wishlist->city_id = (!empty($city)) ? $city:null; 
            $wishlist->keyword = (!empty($keyword)) ? $keyword:null; 
            $wishlist->region = (!empty($region)) ? $region:null; 
            $wishlist->min_price = (!empty($min_p)) ? $min_p:null; 
            $wishlist->max_price = (!empty($max_p)) ? $max_p:null; 
            $wishlist->bedroom = (!empty($bed)) ? $bed:null;  
            $wishlist->bathroom = (!empty($bath)) ? $bath:null;

            $wishlist->save();
            return redirect()->route('wishlist')
            ->with('success', 'Wishlist created successfully.');
        }else{
            return redirect()->route('login');
        }
    }
    public function edit($id)
    {
        if ($id) {
            $data['province'] = Province::all();
            $data['city'] = City::all();
            $data['propertyCategory'] = PropertyCategory::all();

            $wishlist = Wishlist::find($id);
            $param['id'] = $wishlist->id;
            $param['transaction_type'] = $wishlist->type;
            $param['property_type'] = $wishlist->propertycategory_id;
            $param['title'] = $wishlist->title;
            $param['keyword'] = $wishlist->keyword;
            $param['province'] = $wishlist->province_id;
            $param['city'] = $wishlist->city_id;
            $param['region'] = $wishlist->region;
            $param['bed'] = $wishlist->bedroom;
            $param['bath'] = $wishlist->bathroom;
            $param['min_p'] = $wishlist->min_price;
            $param['max_p'] = $wishlist->max_price;

            $data['param'] = $param;
            return view('pages.wishlist.edit', $data);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vidio  $vidio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $transaction_type = $request->input('type');
        $property_type = $request->input('propertyCategory');
        $title = $request->input('title');
        $keyword = $request->input('keyword');
        $province = $request->input('province');
        $city = $request->input('city');
        $is_wishlist = $request->input('is_wishlist');
        $region = $request->input('region');
        $bed = $request->input('bed');
        $bath = $request->input('bath');
        $min_p = $request->input('min_price');
        $max_p = $request->input('max_price');

        if (Auth::check()) {
            $wishlist = Wishlist::find($id);
            $wishlist->user_id = Auth::user()->id;
            $wishlist->title = (!empty($title)) ? $title:null;
            $wishlist->type = (!empty($transaction_type)) ? $transaction_type:null; 
            $wishlist->propertycategory_id = (!empty($property_type)) ? $property_type:null; 
            $wishlist->province_id = (!empty($province)) ? $province:null; 
            $wishlist->city_id = (!empty($city)) ? $city:null; 
            $wishlist->keyword = (!empty($keyword)) ? $keyword:null; 
            $wishlist->region = (!empty($region)) ? $region:null; 
            $wishlist->min_price = (!empty($min_p)) ? $min_p:null; 
            $wishlist->max_price = (!empty($max_p)) ? $max_p:null; 
            $wishlist->bedroom = (!empty($bed)) ? $bed:null;  
            $wishlist->bathroom = (!empty($bath)) ? $bath:null;

            $wishlist->save();
            return redirect()->route('wishlist')
            ->with('success', 'Wishlist updated successfully.');
        }else{
            return redirect()->route('login');
        }
    }

    public function destroy($id)
    {
        $wishlist = Wishlist::find($id)->delete();
        return redirect()->route('wishlist')
                        ->with('success','Wishlist deleted successfully');
    }
    public function addToWishlist(Request $request) {
        // echo "sukses";
        $transaction_type = $request->input('type');
        $property_type = $request->input('propertyCategory');
        $title = $request->input('title');
        $keyword = $request->input('keyword');
        $province = $request->input('province');
        $city = $request->input('city');
        $region = $request->input('region');
        $bed = $request->input('bed');
        $bath = $request->input('bath');
        $min_p = $request->input('min_price');
        $max_p = $request->input('max_price');

        if ($transaction_type!="") {
                if (Auth::check()) {
                    $wishlist = new Wishlist();
                    $wishlist->user_id = Auth::user()->id;
                    $wishlist->title = (!empty($title)) ? $title:null;
                    $wishlist->type = (!empty($transaction_type)) ? $transaction_type:null; 
                    $wishlist->propertycategory_id = (!empty($property_type)) ? $property_type:null; 
                    $wishlist->province_id = (!empty($province)) ? $province:null; 
                    $wishlist->city_id = (!empty($city)) ? $city:null; 
                    $wishlist->keyword = (!empty($keyword)) ? $keyword:null; 
                    $wishlist->region = (!empty($region)) ? $region:null; 
                    $wishlist->min_price = (!empty($min_p)) ? $min_p:null; 
                    $wishlist->max_price = (!empty($max_p)) ? $max_p:null; 
                    $wishlist->bedroom = (!empty($bed)) ? $bed:null;  
                    $wishlist->bathroom = (!empty($bath)) ? $bath:null;  

                    $wishlist->save();
                }
            }
            return back()->withInput();
    }
     public function addToWishlistModal(Request $request) {
        // echo "sukses";
        $transaction_type = $request->input('typem');
        $property_type = $request->input('propertyCategorym');
        $title = $request->input('titlem');
        $keyword = $request->input('keywordm');
        $province = $request->input('provincem');
        $city = $request->input('citym');
        $region = $request->input('regionm');
        $bed = $request->input('bedm');
        $bath = $request->input('bathm');
        $min_p = $request->input('min_pricem');
        $max_p = $request->input('max_pricem');

        if ($transaction_type!="") {
                if (Auth::check()) {
                    $wishlist = new Wishlist();
                    $wishlist->user_id = Auth::user()->id;
                    $wishlist->title = (!empty($title)) ? $title:null;
                    $wishlist->type = (!empty($transaction_type)) ? $transaction_type:null; 
                    $wishlist->propertycategory_id = (!empty($property_type)) ? $property_type:null; 
                    $wishlist->province_id = (!empty($province)) ? $province:null; 
                    $wishlist->city_id = (!empty($city)) ? $city:null; 
                    $wishlist->keyword = (!empty($keyword)) ? $keyword:null; 
                    $wishlist->region = (!empty($region)) ? $region:null; 
                    $wishlist->min_price = (!empty($min_p)) ? $min_p:null; 
                    $wishlist->max_price = (!empty($max_p)) ? $max_p:null; 
                    $wishlist->bedroom = (!empty($bed)) ? $bed:null;  
                    $wishlist->bathroom = (!empty($bath)) ? $bath:null;  

                    $wishlist->save();
                }
            }
        return back()->withInput();
    }
}
