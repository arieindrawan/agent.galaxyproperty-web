<?php

namespace App\Http\Controllers;

use App\Models\PropertyOpenHouse;
use App\Models\Province;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use DB;
use PDF;

class OpenHouseController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['property'] = PropertyOpenHouse::all();
        //$region = propertyOpenHouse::getRegion()->paginate(15);

        $region = DB::table('property_openhouses')->select(DB::raw('distinct(region) as region'))->paginate(15);
        $data['region'] = $region;

        return view('pages.open_house.index', $data);
    }

    public function listProperty()
    {
        $property = PropertyOpenHouse::all();
        foreach ($property as $key => $value) {
            $str_img = json_decode($value->images,true);
            $value->path_images = $str_img;
        }
        $data['property'] = $property;
        $data['region'] = propertyOpenHouse::getRegion();
        return view('pages.open_house.list_property', $data);
    }
    public function cetakListing()
    {
        $property = PropertyOpenHouse::all();
        foreach ($property as $key => $value) {
            $str_img = json_decode($value->images,true);
            $value->path_images = $str_img;
        }
        $data['property'] = $property;
        $data['region'] = propertyOpenHouse::getRegion();
        return view('pages.open_house.cetak_listing', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        date_default_timezone_set("Asia/Jakarta");

        $start_hour = "2020-10-20";
        $end_hour =  "2040-09-01";

        $param_data = [];
        $param_data['start_hour'] = $start_hour;
        $param_data['end_hour'] = $end_hour;
        $param_data['description'] = '#OH2020';
        $param_data['order'] = 'N2O';
        $param_data['page'] = 1;
        $perPage = 0;
        $param_data['limit'] = $perPage;
        // $param_data['limit'] = 0;
        $dataApiProperty =  Http::get('https://galaxyproperty.co.id/property/getPropertyAPI',$param_data);
        $dataApiProperty = json_decode($dataApiProperty);
        $data_property = $dataApiProperty->property;

        $propertyFix = [];
        $arr_id_property_openhouse = PropertyOpenHouse::getAllID();
        $arrid = array();
        foreach ($arr_id_property_openhouse as $key => $value) {
            array_push($arrid, $value->id);
        }
        foreach ($data_property as $key => $value) {
            if (!(in_array( $value->id, $arrid))) {
                $str_img = json_decode($value->images,true);
                $value->path_images = $str_img;
                $propertyFix[] = $value;
            }

        }

        //dump($data_property);
        $data['property'] = $propertyFix;
        return view('pages.open_house.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $reques,$id)
    {
        if ($id) {
            $param_api['id'] = $id;
            $dataApiProperty =  Http::get('https://galaxyproperty.co.id/property/getPropertyAPI',$param_api);
            $dataApiProperty = json_decode($dataApiProperty);
            $data_property = $dataApiProperty->property;
            //dump($dataApiProperty);
            $data_property = $data_property[0];
            $property['id'] = $data_property->id;
            $property['number']  = $data_property->number;
            $property['title'] = $data_property->title;
            $property['description'] = $data_property->description;
            $property['price'] = $data_property->price;
            $property['land_area'] = $data_property->land_area;
            $property['building_area'] = $data_property->building_area;
            $property['listing_type'] = $data_property->listing_type;
            $property['start_date'] = $data_property->start_date;
            $property['end_date'] = $data_property->end_date;
            $property['transaction_type'] = $data_property->transaction_type;
            $property['propertycategory_id'] = $data_property->propertycategory_id;
            $property['certificate_id'] = $data_property->certificate_id;
            $property['province_id'] = $data_property->province_id;
            $property['city_id'] = $data_property->city_id;
            $property['district_id'] = $data_property->district_id;
            $property['region'] = $data_property->region;
            $property['address'] = $data_property->address;
            $property['toward'] = $data_property->toward;
            $property['bedroom'] = $data_property->bedroom;
            $property['bathroom'] = $data_property->bathroom;
            $property['has_additional_bedroom'] = $data_property->has_additional_bedroom;
            $property['has_additional_bathroom'] = $data_property->has_additional_bathroom;
            $property['floor'] = $data_property->floor;
            $property['status'] = $data_property->status;
            $property['images'] = $data_property->images;
            $property['gallery'] = $data_property->gallery;
            $property['latitude'] = $data_property->latitude;
            $property['longitude'] = $data_property->longitude;
            $property['is_hot'] = $data_property->is_hot;
            $property['is_hot_active'] = $data_property->is_hot_active;
            $property['hot_ordering'] = $data_property->hot_ordering;
            $property['approve_hot_at'] = $data_property->approve_hot_at;
            $property['type'] = $data_property->type;
            $property['knowledge'] = $data_property->knowledge;
            $property['block'] = $data_property->block;
            $property['alley'] = $data_property->alley;
            $property['house_number'] = $data_property->house_number;
            $property['additional_info'] = $data_property->additional_info;
            $property['nonactivereason_id'] = $data_property->nonactivereason_id;
            $property['nonactive_note'] = $data_property->nonactive_note;
            $property['trx_id'] = $data_property->trx_id;
            $property['brochure'] = $data_property->brochure;

            PropertyOpenHouse::create($property);
            return back()->withInput()->with('success', 'Property berhasil ditambahkan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PropertyOpenHouse  $propertyOpenHouse
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyOpenHouse $propertyOpenHouse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PropertyOpenHouse  $propertyOpenHouse
     * @return \Illuminate\Http\Response
     */
    public function edit(PropertyOpenHouse $propertyOpenHouse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PropertyOpenHouse  $propertyOpenHouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropertyOpenHouse $propertyOpenHouse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PropertyOpenHouse  $propertyOpenHouse
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $propertyOpenHouse = PropertyOpenHouse::where('id_property','=',$id)->delete();
        return back()->withInput()->with('success', 'Property berhasil dihapus.');
        
    }
    public function export($id){
        $id_property_print = json_decode($id);
        $property = PropertyOpenHouse::all();

        $propertyFix = [];
        foreach ($property as $key => $value) {
            if (in_array( $value->id, $id_property_print)) {
                $str_img = json_decode($value->images,true);
                $value->path_images = $str_img;
                $words = explode(" ",$value->title);
                $value->title = implode(" ",array_splice($words,0,5));
                $propertyFix[] = $value;
            }

        }
                                
        //share data to view
        //view()->share('property',$propertyFix);

        $pdf = PDF::setOptions(['isRemoteEnabled'=>true])->loadView('pages.open_house.listing_pdf',['property'=> $propertyFix]);
        $pdf->setPaper('a4', 'potrait');

        return $pdf->download('listing_galaxy.pdf');
    }
    public function getByRegion($region){

        $property = PropertyOpenHouse::where('region',$region)->paginate(15);
         foreach ($property as $key => $value) {
                $str_img = json_decode($value->images,true);
                $value->path_images = $str_img;
                $words = explode(" ",$value->title);
                $value->title = implode(" ",array_splice($words,0,5));

        }
        $data['property'] = $property;
        //return redirect('/');
        return view('pages.open_house.property_per_region', $data);
    }
    public function getByProvince($province){
        $province = 
        $region = DB::table('property_openhouses')
            ->select('property_openhouses.region')
            ->distinct('property_openhouses.region')
            ->leftJoin('province', 'property_openhouses.province_id', '=', 'province.id')
            ->leftJoin('city', 'property_openhouses.city_id', '=', 'city.id')
            ->where('province.name',$province)
            ->orWhere('city.name',$province)
            ->get();

        $data['region'] = $region;
        return view('pages.open_house.region_per_city', $data);
    }
}
