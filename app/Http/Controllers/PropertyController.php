<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\Province;
use App\Models\City;
use App\Models\PropertyCategory;
use App\Models\Wishlist;
use Illuminate\Pagination\Paginator;
use DB;
use PDF;
use Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Pagination\LengthAwarePaginator;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {       
        $data['province'] = Province::all();
        $data['city'] = City::all();
        $data['propertyCategory'] = PropertyCategory::all();

        $is_search = false;

        $transaction_type = $request->input('type');
        $property_type = $request->input('propertyCategory');
        $title = $request->input('title');
        $keyword = $request->input('keyword');
        $province = $request->input('province');
        $city = $request->input('city');
        $is_wishlist = $request->input('is_wishlist');
        $region = $request->input('region');
        $bed = $request->input('bed');
        $bath = $request->input('bath');
        $min_p = $request->input('min_price');
        $max_p = $request->input('max_price');

        $param['transaction_type'] = $transaction_type;
        $param['property_type'] = $property_type;
        $param['title'] = $title;
        $param['keyword'] = $keyword;
        $param['province'] = $province;
        $param['city'] = $city;
        $param['region'] = $region;
        $param['bed'] = $bed;
        $param['bath'] = $bath;
        $param['min_p'] = $min_p;
        $param['max_p'] = $max_p;

        if ($transaction_type!="") {
            $is_search = true;
        }
        $data['param'] = $param;
        $data['is_search'] = $is_search;

        $param_api = [];
        if (!empty($transaction_type)) {
                    if ($transaction_type!="all"){
                        $param_api['type'] = $transaction_type;
                    }
                    if ($property_type != ""){
                        $param_api['propertyCategory'] = $property_type; 
                    }
                    if ($title != ""){
                        $param_api['title'] = $title;
                    }
                    if ($keyword != ""){
                        $param_api['keyword'] = $keyword;
                    }
                    if ($province != ""){
                        $param_api['provinceId'] = $province;
                    }
                    if ($city != ""){
                        $param_api['cityId'] = $city;
                    }
                    if ($region != ""){
                        $param_api['region'] = $region;
                    }
                    if ($bed != ""){
                        if ($bed == "3") {
                            $param_api['bed'] = 4;
                        }
                        else{
                            $param_api['bed'] = $bed;
                        }
                    }
                    if ($bath != ""){
                        if ($bath == "3") {
                            $param_api['bath'] = 4;
                        }
                        else{
                            $param_api['bath'] = $bath;
                        }
                    }
                    if ($min_p != ""){
                        $param_api['price_min'] = $min_p;
                    }
                    if ($max_p != ""){
                        if ($max_p!="~") {
                            $param_api['price_max'] = $max_p;
                        }
                        
                    }
                }

            // set current page
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // set limit 
            $perPage = 15;

            $param_api['order'] = 'N2O';
            $param_api['page'] = $currentPage;
            $param_api['limit'] = $perPage;
            //dump($param_api);
            $dataApi =  Http::get('https://galaxyproperty.co.id/property/getPropertyAPI',$param_api);
            $dataApi = json_decode($dataApi);
            //$property = json_decode($property['property']);
            //dump($dataApi->property);
            if ($is_wishlist) {
                if (Auth::check()) {
                    $wishlist = new Wishlist();
                    $wishlist->user_id = Auth::user()->id;
                    $wishlist->title = (!empty($title)) ? $title:null;
                    $wishlist->type = (!empty($transaction_type)) ? $transaction_type:null; 
                    $wishlist->propertycategory_id = (!empty($property_type)) ? $property_type:null; 
                    $wishlist->province_id = (!empty($province)) ? $province:null; 
                    $wishlist->city_id = (!empty($city)) ? $city:null; 
                    $wishlist->keyword = (!empty($keyword)) ? $keyword:null; 
                    $wishlist->region = (!empty($region)) ? $region:null; 
                    $wishlist->min_price = (!empty($min_p)) ? $min_p:null; 
                    $wishlist->max_price = (!empty($max_p)) ? $max_p:null; 
                    $wishlist->bedroom = (!empty($bed)) ? $bed:null;  
                    $wishlist->bathroom = (!empty($bath)) ? $bath:null;  

                    $wishlist->save();
                }
            }
            $property = $dataApi->property;
            
            foreach ($property as $key => $value) {
                $str_img = json_decode($value->images,true);
                $value->path_images = $str_img;
                
            }

            $property = collect($property);
            // generate pagination
            //$currentResults = $property->slice(($currentPage - 1) * $perPage, $perPage)->all();
            $property = new LengthAwarePaginator($property, $dataApi->count, $perPage);
            foreach ($property as $key => $value) {
                $words = explode(" ",$value->title);
                $value->title = implode(" ",array_splice($words,0,5));
            }
            $property->appends($request->all());
            $data['property'] = $property;
        //return $property;
        return view('pages.property.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getCity(Request $request)
    {
        $province_id = $request->province_id;
        $data = City::where('province_id',$province_id)->get();
        return json_encode($data);
    }
    public function getProvince(Request $request)
    {
        $data = Province::all();
        return json_encode($data);
    }
     // Generate PDF
    public function createPDF(Request $request) {
    // retreive all records from db
        $transaction_type = $request->input('type');
        $property_type = $request->input('propertyCategory');
        $title = $request->input('title');
        $keyword = $request->input('keyword');
        $province = $request->input('province');
        $city = $request->input('city');
        $region = $request->input('region');
        $bed = $request->input('bed');
        $bath = $request->input('bath');
        $min_p = $request->input('min_price');
        $max_p = $request->input('max_price');

        $param_api = [];
        if (!empty($transaction_type)) {
                    if ($transaction_type!="all"){
                        $param_api['type'] = $transaction_type;
                    }
                    if ($property_type != ""){
                        $param_api['propertyCategory'] = $transaction_type; 
                    }
                    if ($title != ""){
                        $param_api['title'] = $title;
                    }
                    if ($keyword != ""){
                        $param_api['keyword'] = $keyword;
                    }
                    if ($province != ""){
                        $param_api['provinceId'] = $province;
                    }
                    if ($city != ""){
                        $param_api['cityId'] = $city;
                    }
                    if ($region != ""){
                        $param_api['region'] = $region;
                    }
                    if ($bed != ""){
                        if ($bed == "3") {
                            $param_api['bed'] = 4;
                        }
                        else{
                            $param_api['bed'] = $bed;
                        }
                    }
                    if ($bath != ""){
                        if ($bath == "3") {
                            $param_api['bath'] = 4;
                        }
                        else{
                            $param_api['bath'] = $bath;
                        }
                    }
                    if ($min_p != ""){
                        $param_api['price_min'] = $min_p;
                    }
                    if ($max_p != ""){
                        if ($max_p!="~") {
                            $param_api['price_max'] = $max_p;
                        }
                        
                    }
                }


            // set limit 
            $perPage = 0;

            $param_api['order'] = 'N2O';
            $param_api['limit'] = $perPage;
            //dump($param_api);
            $dataApi =  Http::get('https://galaxyproperty.co.id/property/getPropertyAPI',$param_api);
            $dataApi = json_decode($dataApi);

            $property = $dataApi->property;
            
            foreach ($property as $key => $value) {
                $str_img = json_decode($value->images,true);
                $value->path_images = $str_img;
                $words = explode(" ",$value->title);
                $value->title = implode(" ",array_splice($words,0,5));
            }
                                
      //share data to view
        view()->share('property',$property);
        $pdf = PDF::loadView('pages.property.property_pdf', $property);
        $pdf->setPaper([0, 0, 660, 850], 'potrait');

      // download PDF file with download method
      return $pdf->download('listing_galaxy.pdf');
    }
    public function getPropertyAPI(){
        // $response = Http::get('http://localhost/eragalaxy/property/getPropertyAPI');
        return Http::get('http://propertilaris.com/property/getPropertyAPI');
    }
}
