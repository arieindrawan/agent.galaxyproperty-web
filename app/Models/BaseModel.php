<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    /*
    protected $table = 'table_name';
    protected $primaryKey = 'id_table';
    */
    public static $initQuery = 'init_query';

    public static function getAll(
        $limit = null, 
        $offset = null, 
        $orderColumn = null, 
        $orderDirection = 'asc', 
        $where = null,
        $whereIn = null,
        $orWhere = null,
        $select = null
    )
    {
        return self::baseQuery(self::setInitQuery(), $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }

    public static function getOne(
        $limit = null, 
        $offset = null, 
        $orderColumn = null, 
        $orderDirection = 'asc', 
        $where = null,
        $whereIn = null,
        $orWhere = null,
        $select = null
    )
    {
        return self::baseQuery(self::setInitQuery(), $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->first();
    }

    public static function baseQuery(
        $query_model,
        $limit = null, 
        $offset = null, 
        $orderColumn = null, 
        $orderDirection = 'asc', 
        $where = null,
        $whereIn = null,
        $orWhere = null,
        $select = null,
        $groupBy = null
    )
    {
        if ($select) {
            $select = json_decode($select,true);
            foreach ($select as $key => $val) {
                if ($key==0) {
                    $query_model->select($val);
                }else{
                    $query_model->addSelect($val);
                }
            }
        }

        if ($where) {
            $where = json_decode($where,true);
            foreach ($where as $key => $value) {
                $query_model->where($value[0], $value[1], $value[2]);
            }
        }
        if ($orWhere) {
            $orWhere = json_decode($orWhere,true);
            foreach ($orWhere as $key => $value) {
                $query_model->orWhere($value[0], $value[1], $value[2]);
            }
        }
        if ($whereIn) {
            $whereIn = json_decode($whereIn,true);
            foreach ($whereIn as $key => $row) {
                // dump($row[0], explode(',', $row[1]));exit();
                $query_model->whereIn($row[0], explode(',', $row[1]));
            }
        }
        if ($groupBy) {
            $query_model->groupBy($groupBy);
        }
        if ($limit) {
            $query_model->limit($limit);
        }
        if ($offset) {
            $query_model->offset($offset);
        }
        if ($orderColumn) {
            $query_model->orderBy($orderColumn, $orderDirection);
        }
        return $query_model;
    }
}
