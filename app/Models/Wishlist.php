<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends BaseModel
{
    use HasFactory;

    protected $table = 'wishlist_requests';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'title',
        'type',
        'propertycategory_id',
        'province_id',
        'city_id',
        'keyword',
        'region',
        'min_price',
        'max_price',
        'bedroom',
        'bathroom',
        'min_land_area',
        'max_land_area',
        'min_building_area',
        'max_building_area'
    ];
    public static function setInitQuery()
    {
        return self::query()
                    ->select(
                        'wishlist_requests.*, pc.name as propertycategory_name, pr.name as province_name, c.name as city_name'
                    )->leftJoin('province as pr', 'wishlist_requests.province_id', '=', 'pr.id')
                    ->leftJoin('city as c', 'wishlist_requests.city_id', '=', 'c.id')
                    ->leftJoin('propertycategory as pc', 'wishlist_requests.propertycategory_id', '=', 'pc.id')
                    ;
    }
    public static function getWihslist($where=null,$limit = null)
    {
        $offset = null;
        $orderColumn = null;
        $orderDirection = 'asc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        if ($where!=null) {
            $where = json_encode($where);
        }
        $query = self::query()
                    ->selectRaw('wishlist_requests.*, pc.name as propertycategory_name, pr.name as province_name, c.name as city_name')
                   ->leftJoin('province as pr', 'wishlist_requests.province_id', '=', 'pr.id')
                    ->leftJoin('city as c', 'wishlist_requests.city_id', '=', 'c.id')
                    ->leftJoin('propertycategory as pc', 'wishlist_requests.propertycategory_id', '=', 'pc.id')
                    ;

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
}
