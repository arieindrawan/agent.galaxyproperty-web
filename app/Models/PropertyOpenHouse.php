<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyOpenHouse extends BaseModel
{
    use HasFactory;
    protected $table = 'property_openhouses';
    public $timestamps = true;
    protected $fillable = [
    	'id',
        'number',
        'title',
        'description',
        'price',
        'land_area',
        'building_area',
        'listing_type',
        'start_date',
        'end_date',
        'transaction_type',
        'propertycategory_id',
        'certificate_id',
        'province_id',
        'city_id',
        'district_id',
        'region',
        'address',
        'toward',
        'bedroom',
        'bathroom',
        'has_additional_bedroom',
        'has_additional_bathroom',
        'floor',
        'status',
        'images',
        'gallery',
        'latitude',
        'longitude',
        'is_hot',
        'is_hot_active',
        'hot_ordering',
        'approve_hot_at',
        'type',
        'knowledge',
        'block',
        'alley',
        'house_number',
        'additional_info',
        'nonactivereason_id',
        'nonactive_note',
        'trx_id',
        'brochure'
    ];

    public static function setInitQuery()
    {
        return self::query()
                    ->select(
                        'property_openhouses.*',
                    );
    }
    public static function getRegion($start=null,$end=null)
    {
        $offset = null;
        $limit = null;
        $orderColumn = 'updated_at';
        $orderDirection = 'desc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        if ($start!=null) {
            $where[] = ['u.created_at','>=',$start.''];
            
        }
        if ($end!=null) {
            $where[] = ['u.created_at','<=',$end.''];
            
        }
        if ($where !=null) {
            $where = json_encode($where);
        }
        $query = self::query()
                    ->selectRaw('DISTINCT(region)as region');

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
    public static function getAllID($start=null,$end=null)
    {
        $offset = null;
        $limit = null;
        $orderColumn = 'updated_at';
        $orderDirection = 'desc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        if ($start!=null) {
            $where[] = ['u.created_at','>=',$start.''];
            
        }
        if ($end!=null) {
            $where[] = ['u.created_at','<=',$end.''];
            
        }
        if ($where !=null) {
            $where = json_encode($where);
        }
        $query = self::query()
                    ->selectRaw('DISTINCT(id)as id');

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
}
