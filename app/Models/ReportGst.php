<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportGst extends BaseModel
{
    use HasFactory;
    protected $table = 'report_gsts';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'vidio_id',
        'opened_at',
        'is_done'
    ];
    public static function setInitQuery()
    {
        return self::query()
                    ->select(
                        'report_gsts.*',
                        'u.name as agent_name',
                        'v.name as vidio_name'
                    )
                    ->leftJoin('users as u', 'u.id', '=', 'report_gsts.user_id')
                    ->leftJoin('vidios as v', 'v.id', '=', 'report_gsts.vidio_id')
                    ;
    }
    public static function getAgentReport($start =null,$end= null,$limit = null)
    {
        $offset = null;
        $orderColumn = 'u.name';
        $orderDirection = 'asc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        if ($start!=null) {
            $where[] = ['u.created_at','>=',$start.''];
            
        }
        if ($end!=null) {
            $where[] = ['u.created_at','<=',$end.''];
            
        }
        if ($where !=null) {
            $where = json_encode($where);
        }
        $query = self::query()
                    ->selectRaw('u.id as user_id,u.name as agent_name,count(DISTINCT(vidio_id))as vidio_ditonton')
                    ->rightJoin('users as u', 'u.id', '=', 'report_gsts.user_id')->groupBy('u.id')->groupBy('u.name');

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
    public static function getVidioReport($limit = null)
    {
        $offset = null;
        $orderColumn = 'v.name';
        $orderDirection = 'asc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        $query = self::query()
                    ->selectRaw('v.id as vidio_id,v.name as vidio_name,count(DISTINCT(user_id))as user_menonton')
                    ->rightJoin('vidios as v', 'v.id', '=', 'report_gsts.vidio_id')->groupBy('v.id')->groupBy('v.name');

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
    public static function getStatisticReport($limit = null,$select_p,$start,$end)
    {
        $offset = null;
        $orderColumn = null;
        $orderDirection = 'asc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        $query = self::query()
                    ->selectRaw($select_p)
                    ->whereBetween('HOUR(opened_at)', [$start, $end]);

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
    public static function getDetailAgent($id=null,$limit=null)
    {
        $offset = null;
        $orderColumn = 'v.name';
        $orderDirection = 'asc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        if ($id!=null) {
            $where[] = ['report_gsts.user_id','=',$id];
            $where = json_encode($where);
        }
        $query = self::query()
                    ->selectRaw('report_gsts.vidio_id as vidio_id,v.name as vidio_name,count(user_id) as user_menonton')
                    ->leftJoin('vidios as v', 'v.id', '=', 'report_gsts.vidio_id')
                    ->groupBy('report_gsts.vidio_id')
                    ->groupBy('report_gsts.user_id')
                    ->groupBy('v.name');

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
    public static function getDetailVidio($id=null,$limit=null)
    {
        $offset = null;
        $orderColumn = 'u.name';
        $orderDirection = 'asc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        if ($id!=null) {
            $where[] = ['report_gsts.vidio_id','=',$id];
            $where = json_encode($where);
        }
        $query = self::query()
                    ->selectRaw('report_gsts.user_id as user_id,u.name as user_name,count(vidio_id) as user_menonton')
                    ->leftJoin('users as u', 'u.id', '=', 'report_gsts.user_id')
                    ->groupBy('report_gsts.user_id')
                    ->groupBy('report_gsts.vidio_id')
                    ->groupBy('u.name');

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }

    

}
