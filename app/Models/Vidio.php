<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vidio extends BaseModel
{
    use HasFactory;

    protected $table = 'vidios';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'code',
        'duration',
        'link_url',
        'order'
    ];
}
