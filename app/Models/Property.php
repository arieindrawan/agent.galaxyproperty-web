<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends BaseModel
{
    use HasFactory;
    protected $table = 'property';

    public static function setInitQuery()
    {
        return self::query()
                    ->select(
                        'property.*, pc.name as propertycategory_name, pr.name as province_name, c.name as city_name, m.id AS marketing_id'
                    )->leftJoin('province as pr', 'property.province_id', '=', 'pr.id')
                    ->leftJoin('city as c', 'property.city_id', '=', 'c.id')
                    ->leftJoin('propertycategory as pc', 'property.propertycategory_id', '=', 'pc.id')
                    ->leftJoin('propertylisting as pl', 'pl.property_id', '=', 'property.id')
                    ->leftJoin('marketing as m', 'pl.marketing_id', '=', 'm.id')
                    ->where('property.is_deleted', '=', 0)
                    ;
    }
    public static function getProperty($where=null,$limit = null)
    {
        $offset = null;
        $orderColumn = 'property.created_at';
        $orderDirection = 'desc';
        $whereIn = null;
        $orWhere = null;
        $select = null;
        $where = null;
        $query = self::query()
                    ->selectRaw('property.*, pc.name as propertycategory_name, pr.name as province_name, c.name as city_name, m.id AS marketing_id')
                    ->leftJoin('province as pr', 'property.province_id', '=', 'pr.id')
                    ->leftJoin('city as c', 'property.city_id', '=', 'c.id')
                    ->leftJoin('propertycategory as pc', 'property.propertycategory_id', '=', 'pc.id')
                    ->leftJoin('propertylisting as pl', 'pl.property_id', '=', 'property.id')
                    ->leftJoin('marketing as m', 'pl.marketing_id', '=', 'm.id')
                    ->where('property.is_deleted', '=', 0)
                    ;

        return self::baseQuery($query, $limit, $offset, $orderColumn, $orderDirection, $where, $whereIn, $orWhere, $select)
                    ->get();
    }
}
