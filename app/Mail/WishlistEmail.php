<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class WishlistEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->from('devgalaxyproperty@gmail.com')
                    ->view('pages.mail.wishlist_email')
                    ->with(
                    [
                        'nama' => 'AGENT GALAXY',
                        'website' => 'www.galaxyproperty.co.id',
                        'data_property' => ($this->data['property']),
                        'data_param' => ($this->data['param']),
                        'data_agent' => User::find($this->data['data_wishlist']->user_id),
                    ]);
    }
}
