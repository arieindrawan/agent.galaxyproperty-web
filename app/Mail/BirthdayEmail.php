<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use PDF;

class BirthdayEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // /return $this->view('view.name');
        $pc =  $this->data['user'];
        $pdf = PDF::loadView('pages.mail.birthday_certificate',['data_agent' => $this->data['user']]);
        $pdf->setPaper([0, 0, 460, 770], 'potrait');
        $pdf->save('public/birthday-certificate/'.$pc->name.'-'.$pc->birthdate.'.pdf');


        return $this->from('devgalaxyproperty@gmail.com')
                    ->view('pages.mail.birthday_email')
                    ->attach('public/birthday-certificate/'.$pc->name.'-'.$pc->birthdate.'.pdf')
                    ->with(
                    [
                        'data_agent' => $this->data['user'],
                    ]);
    }
}
